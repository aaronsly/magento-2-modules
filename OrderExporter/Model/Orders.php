<?php
/**
 * Copyright © Essential E-Commerce Ltd, All rights reserved.
 */

namespace Essential\OrderExporter\Model;

/**
 * Essential Order Exporter Model
 * Used to get data saved in the essential_order_export table
 *
 * @version 1.0.0
 */

class Orders extends \Magento\Framework\Model\AbstractModel
{
	/**
     * Helper instance
     * @var \Essential\OrderExporter\Helper\Data
     */
	protected $_helper;
	
	/**
     * Logging instance
     * @var \Essential\OrderExporter\Logger\Logger
     */
	protected $_logger;
	
	/**
	 * @var Magento\Sales\Model\ResourceModel\Order\Status\Collection
	 */
	private $_statusCollection;

	/**
	 * @var array|null
	 */
	private $status = null;
	
	public function __construct(
		\Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
		\Essential\OrderExporter\Helper\Data $helperData,
		\Essential\OrderExporter\Logger\Logger $logger,
		\Magento\Sales\Model\ResourceModel\Order\Status\Collection $statusCollection,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = [])		
	{		
		parent::__construct($context,$registry);
		$this->_statusCollection = $statusCollection;
		$this->_helper = $helperData;
		$this->_logger = $logger;
	}
	
	/**
	* Define resource model
	*/
	public function _construct()
	{
		$this->_init('Essential\OrderExporter\Model\Resource\Orders');
	}
	
	/**
	* Get all order information saved in table
	*
	* return array
	*/
	public function getAllOrders()
	{
		return $this->getCollection()->getData();
	}
	
	/**
	* Get all order information by order id
	*
	* @var $id string, int
	*
	* return array
	*/
	public function getOrderById($id)
	{	
		try {
			
			$result = $this->getCollection()->addFieldToFilter('order_id',$id)->getData();
			
			if (empty($result))	{
				
				return false;
				
			} else {
				
				return reset($result);
				
			}
			
		} catch (Exception $e) {
			
			$this->_logger->writeInfo("Unable to load previously imported order by id", $id, null, $e->getMessage());
			return "error";
			
		}		
		 
	}
	
	/**
	* Save order data to table
	*
	* @var $id int
	* @var $orderId string, int
	* @var $attempts int
	* @var $exported bool
	* @var $ignore bool
	* @var $supplier array
	*
	* return bool
	*/
	public function saveOrder($id, $orderId, $attempts = null, $exported = null, $ignore = null, $supplier = null)
	{
		try {
			
			// Set table id
			if (!is_null($id)){
				$this->setId($id);
			} else {
				$savedOrder = $this->getOrderById($orderId);
				if(isset($savedOrder['id']))
					$this->setId($savedOrder['id']);
			}		
			
			// Set order id
			$this->setOrderId($orderId);
			// Set export attempts
			if(!is_null($attempts))
				$this->setExportAttempts($attempts);
			
			if(!is_null($exported))
				$this->setExported($exported);
			// Set ignore
			$this->setIgnore($ignore);
			// Set supplier export status
			if (!is_null($supplier) && is_array($supplier)) {

				if($supplier[0] == 'distinctive')
					$this->setDistinctiveExported($supplier[1]);				
				
				if($supplier[0] == 'west')
					$this->setWestExported((bool) $supplier[1]);

				if($supplier[0] == 'towelrads')
					$this->setTowelradsExported((bool) $supplier[1]);
			}
			
			$this->save();
			$this->unsetData();
			
			if ($this->_helper->loggingEnabled())
				$this->_logger->writeInfo("Order saved successfully", $id);
			
			return true;
			
		} catch (Exception $e){
			
			$this->_logger->writeError("Unable to save order to database", $id, null, $e->getMessage());
			return false;
			
		}		
	}
	
	/**
	 * @return array
	 */
	public function getAllStatus() {
		if ($this->status === null) {
			$this->status = [];
			foreach ($this->_statusCollection->toOptionArray() as $status) {
				$this->status[$status['value']] = $status['label'];
			}
		}
		return $this->status;
	}
	
}
