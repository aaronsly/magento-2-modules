<?php
/**
 * Copyright © Essential E-Commerce Ltd, All rights reserved.
 */

namespace Essential\OrderExporter\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

/**
 * Essential Order Exporter Helper
 * @version 1.0.0
 */

class Data extends AbstractHelper
{

	const XML_PATH_ORDER_EXPORTER = 'orderExporter/';
	const SANDBOX_URL = 'http://www.dev.distinctive-wholesale.com/client-portal/ajax/orders/process/process-order-tr.php';
	const LIVE_URL = 'https://www.distinctive-wholesale.com/client-portal/ajax/orders/process/process-order-tr.php';
	
	/**
	* Get config value
	*
	* @var $field string
	* @var $storeId int
	*
	* return string
	*/
	public function getConfigValue($field, $storeId = null)
	{
		return $this->scopeConfig->getValue(
			$field, ScopeInterface::SCOPE_STORE, $storeId
		);
	}
	
	/**
	* Get config value by field id
	*
	* @var $field string
	* @var $storeId int
	*
	* return string
	*/
	public function getConfig($code, $storeId = null)
	{
		return $this->getConfigValue(self::XML_PATH_ORDER_EXPORTER . $code, $storeId);
	}
	
	/**
	* Enable Logging
	*
	* return bool
	*/
	public function loggingEnabled()
	{		
		$val = $this->getConfig('general/enable_logging');
		return filter_var($val, FILTER_VALIDATE_BOOLEAN);
	}
	
	/**
	* Enable Verbose Logging
	*
	* return bool
	*/
	public function verboseLogging()
	{		
		$val = $this->getConfig('general/verbose_logging');
		return filter_var($val, FILTER_VALIDATE_BOOLEAN);
	}
	
	/**
	* Get Error Emails
	*
	* return string
	*/
	public function getErrorEmails()
	{		
		return $this->getConfig('general/email_errors');
	}
	
	/**
	* Enabled Order Exports
	*
	* return bool
	*/
	public function exportsEnabled()
	{
		$val = $this->getConfig('exportOrders/enable_exports');
		return filter_var($val, FILTER_VALIDATE_BOOLEAN);
	}
	
	/**
	* Distinctive Exports Enabled
	*
	* return bool
	*/
	public function distinctiveExportsEnabled()
	{
		$val = $this->getConfig('exportOrders/enable_distinctive');
		return filter_var($val, FILTER_VALIDATE_BOOLEAN);
	}
	
	/**
	* West Exports Enabled
	*
	* return bool
	*/
	public function westExportsEnabled()
	{
		$val = $this->getConfig('exportOrders/enable_west');
		return filter_var($val, FILTER_VALIDATE_BOOLEAN);
	}
	
	/**
	* Get West Emails
	*
	* return string
	*/
	public function getWestEmails()
	{		
		return $this->getConfig('exportOrders/west_emails');
	}

	/**
	* Towelrads Exports Enabled
	*
	* return bool
	*/
	public function towelradsExportsEnabled()
	{
		$val = $this->getConfig('exportOrders/enable_towelrads');
		return filter_var($val, FILTER_VALIDATE_BOOLEAN);
	}
	
	/**
	* Get Towelrads Emails
	*
	* return string
	*/
	public function getTowelradsEmails()
	{		
		return $this->getConfig('exportOrders/towelrads_emails');
	}
	
	/**
	* Get Order Statuses to Import
	*
	* return array
	*/
	public function getExportStatuses()
	{
		$statuses = $this->getConfig('exportOrders/status_select');
		$statuses = explode(",",$statuses);
		return $statuses;	
	}	
	
	/**
	* Use Sandbox Url For Order Exports
	*
	* return bool
	*/
	public function sandboxEnabled()
	{
		$val = $this->getConfig('exportOrders/enable_sandbox');
		return filter_var($val, FILTER_VALIDATE_BOOLEAN);
	}
	
	/**
	* Get Sandbox Emails Emails
	*
	* return string
	*/
	public function getSandboxEmails()
	{		
		return $this->getConfig('exportOrders/sandbox_emails');
	}
	
	
	/**
	* Use External Cron Job For Order Exports
	*
	* return bool
	*/
	public function orderExternalCron()
	{		
		$val = $this->getConfig('exportOrders/external_cron');
		return filter_var($val, FILTER_VALIDATE_BOOLEAN);
	}
	
	/**
	* Get Sandbox Url For Request
	*
	* return string
	*/
	public function getSandboxUrl()
	{
		return self::SANDBOX_URL;
	}
	
	/**
	* Get Live Url For Request
	*
	* return string
	*/
	public function getLiveUrl()
	{
		return self::LIVE_URL;
	}
	
	/**
	* Enable Stock Feed
	*
	* return bool
	*/
	public function stockFeedEnabled()
	{
		$val = $this->getConfig('stockFeed/enable_stock_feed');
		return filter_var($val, FILTER_VALIDATE_BOOLEAN);
	}
	
	/**
	* Use External Cron Job For Stock Feed
	*
	* return bool
	*/
	public function stockExternalCron()
	{		
		$val = $this->getConfig('stockFeed/external_cron');
		return filter_var($val, FILTER_VALIDATE_BOOLEAN);
	}
	
	/**
	* Use External Cron Job For Variants Feed
	*
	* return bool
	*/
	public function variantsExternalCron()
	{		
		$val = $this->getConfig('variantsFeed/external_cron');
		return filter_var($val, FILTER_VALIDATE_BOOLEAN);
	}
	
	/**
	* Get Link To Distinctive Wholesale Stock Feed File
	*
	* return string
	*/
	public function getStockFeedPath() {
		$rawPath = $this->getConfig('stockFeed/stock_feed_path');
		
		if ( substr($rawPath, 0, 1) == '/'){
			$path = $rawPath;
		} else {
			$path = '/' . $rawPath;
		}
		return $path;
	}	

	/**
	* Get Link To West Radiators Stock Feed File
	*
	* return string
	*/
	public function getWestStockFeedPath() {
		$rawPath = $this->getConfig('stockFeed/west_stock_feed_path');
		
		if ( substr($rawPath, 0, 1) == '/'){
			$path = $rawPath;
		} else {
			$path = '/' . $rawPath;
		}
		return $path;
	}

	/**
	* Get Link To Towelrads Radiators Stock Feed File
	*
	* return string
	*/
	public function getTowelradsStockFeedPath() {
		$rawPath = $this->getConfig('stockFeed/towelrads_stock_feed_path');
		
		if ( substr($rawPath, 0, 1) == '/'){
			$path = $rawPath;
		} else {
			$path = '/' . $rawPath;
		}
		return $path;
	}

	/**
	* Get Stock feed email address
	*
	* return string
	*/
	public function getStockFeedEmail() {
		return $this->getConfig('stockFeed/west_stock_feed_user');
	}

	/**
	* Get Stock feed email password
	*
	* return string
	*/
	public function getStockFeedPassword() {
		return $this->getConfig('stockFeed/west_stock_feed_pass');
	}
}
