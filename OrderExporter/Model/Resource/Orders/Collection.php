<?php
/**
 * Copyright © Essential E-Commerce Ltd, All rights reserved.
 */

namespace Essential\OrderExporter\Model\Resource\Orders;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
	/**
	* Define model & resource model
	*/
	protected function _construct()
	{
		$this->_init(
			'Essential\OrderExporter\Model\Orders',
			'Essential\OrderExporter\Model\Resource\Orders'
		);
	}
}