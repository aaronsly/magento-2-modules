<?php
namespace Essential\LiveStock\Block;
class Stock extends \Magento\Framework\View\Element\Template
{
    public $_confProd;
    public $_helper;
    protected $_registry;
    public $_jsonEncoder;

    /**
    * @var \Magento\CatalogInventory\Api\StockRegistryInterface
    */
    private $_stockRegistry;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\ConfigurableProduct\Block\Product\View\Type\Configurable $confProd,
        \Magento\ConfigurableProduct\Helper\Data $helper,
        \Magento\Framework\Registry $registry,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder
        )
	{
        parent::__construct($context);
        $this->_confProd = $confProd;
        $this->_helper = $helper;
        $this->_registry = $registry;
        $this->_jsonEncoder = $jsonEncoder;
        $this->_stockRegistry = $stockRegistry;
	}
    
    public function getChildProducts() {
        return $this->_confProd->getProduct()->getTypeInstance()->getUsedProducts($this->_confProd->getProduct());
    }

    public function configLiveStock() {
        
        if ($this->productType() != 'configurable')
            return false;

        $options = $this->_helper->getOptions( $this->_confProd->getProduct(), $this->_confProd->getAllowProducts());

        $config = [];
        foreach( $this->getChildProducts() as $child) {
            $childId = $child->getId();
            $key = reset($options['index'][$childId]);
            $sl = $this->getStockLevel($childId);

            $config[$key] = [
                'stock' => $sl,
                'message' => $sl > 5 || $sl <= 0 ? false : 'Less than <span>5</span> in stock',
                'product_id' => $childId,
                'super_attr' => $key
            ];
        }
        return $this->_jsonEncoder->encode($config);
    } 

    public function getCurrentProduct() {       
        return $this->_registry->registry('current_product');
    }

    public function getSimpleStock() {
        $product = $this->getCurrentProduct();
        return $this->getStockLevel( $product->getId() );
    }

    public function getStockLevel($id) {
        return (int) $this->_stockRegistry->getStockItem($id)->getQty();
    }

    public function productType() {
        return $this->getCurrentProduct()->getTypeId();
    }
}