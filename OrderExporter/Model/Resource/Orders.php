<?php
/**
 * Copyright © Essential E-Commerce Ltd, All rights reserved.
 */

namespace Essential\OrderExporter\Model\Resource;

class Orders extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
	/**
	* Define main table
	*/
	protected function _construct()
	{
		$this->_init('essential_order_exporter', 'id');
	}
}