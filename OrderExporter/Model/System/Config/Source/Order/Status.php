<?php
/**
 * Copyright © Essential E-Commerce Ltd, All rights reserved.
 */

namespace Essential\OrderExporter\Model\System\Config\Source\Order;
 
class Status implements \Magento\Framework\Option\ArrayInterface
{
	protected $_statusData;

    /**
     * @param \Magento\Sales\Model\Order\Status $statusData
     */
    public function __construct(\Magento\Sales\Model\Order\Status $statusData)
    {
        $this->_statusData = $statusData;
    }

	public function toOptionArray() {
		$statusArray      = array();
		$statusCollection = $this->_statusData->getResourceCollection()->getData();
		foreach ($statusCollection as  $status) {
			$statusArray[] = array("value" => $status["status"], "label" => $status["label"]);
		}
		return $statusArray;
	}
}