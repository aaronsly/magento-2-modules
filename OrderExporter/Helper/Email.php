<?php
/**
 * Copyright © Essential E-Commerce Ltd, All rights reserved.
 */

namespace Essential\OrderExporter\Helper;
 
/**
 * Essential Order Exporter Email Helper
 * @version 1.0.0
 */

class Email extends \Magento\Framework\App\Helper\AbstractHelper
{
    const XML_PATH_ORDER_EXPORTER = 'orderExporter/';
 
 
    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
 
    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    protected $_inlineTranslation;
 
    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $_transportBuilder;
     
    /**
     * @var string
    */
    protected $_templateId;
	
	/**
     * @var string
    */
	public $_storeEmail;
	
	/**
     * @var string
    */
	public $_storeName;
	
	/**
	* @var mixed
	*/
    public $_errorEmails;
 
	
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {

        $this->_storeManager = $storeManager;
		parent::__construct($context);
		$this->_storeEmail = $this->getConfigValue('trans_email/ident_general/email');
		$this->_storeName = $this->getConfigValue('trans_email/ident_general/name');
		$this->_errorEmails = $this->getErrorEmails();
		
    }
 
    /**
     * Return store configuration value
     *
     * @param string $path
     * @param int $storeId
     * @return mixed
     */
    protected function getConfigValue($path, $storeId = null)
    {
        return $this->scopeConfig->getValue(
            $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }
	
	/**
     * Returns email addresses to receive error emails
     *
     * @param int $storeId
     * @return mixed
     */
	public function getErrorEmails($storeId = null)
	{
		$rawEmails = $this->getConfigValue(self::XML_PATH_ORDER_EXPORTER . 'general/email_errors', $storeId);
		
		if (is_null($rawEmails) || empty($rawEmails)) {
			
			return false;
			
		} else {
			
			$rawEmails = explode(",",$rawEmails);
			$emails = [];
			
			foreach ($rawEmails as $email) {
				
				$email = filter_var($email, FILTER_SANITIZE_EMAIL);

				if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
					
					$emails[] = $email;
					
				} else {
					
					$this->_logger->info("$email is not a valid email - Check Essential Order Exporter config settings");
					
				}					
				
			}
			
			return $emails;
			
		}		
	}
 
    /**
     * Return store 
     *
     * @return Store
     */
    public function getStore()
    {
		
        return $this->_storeManager->getStore();
		
    }
  
    /**
     * Send Error Email
	 *
     * @param mixed $vars
	 * @param string $templateId
     */
    public function sendErrorEmails($vars)
    {
		
		if ($this->_errorEmails) {
			            
            return $this->sendEmail( $vars,$this->_errorEmails );
			
		}
		
    }	
	
	/**
     * Send Emails
	 *
     * @param mixed $vars
	 * @param string $templateId
     */
    public function sendEmail($vars, $addresses, $att = null)
    {				
        
		$mail = new \PHPMailer\PHPMailer\PHPMailer();

        try {
            //Server settings
            $mail->SMTPDebug = 0;
            $mail->isSMTP();
            $mail->Host       = 'smtp.gmail.com';
            $mail->SMTPAuth   = true;
            $mail->Username   = $this->getConfigValue(self::XML_PATH_ORDER_EXPORTER . 'general/admin_email', null);
            $mail->Password   = $this->getConfigValue(self::XML_PATH_ORDER_EXPORTER . 'general/admin_password', null);
            $mail->SMTPSecure = 'ssl';
            $mail->Port       = 465;

            //Recipients
            $mail->setFrom('info@traderadiators.com', 'Trade Radiators');

            foreach($addresses as $address) {
                $mail->addAddress($address);
            }     
            
            $mail->addReplyTo('info@traderadiators.com', 'Trade Radiators');

            // Attachments
            if( !is_null( $att ) )
                $mail->addAttachment($att);

            // Content
            $mail->isHTML(true);
            $mail->Subject = $vars['subject'];
            $mail->Body    = $vars['message'];
            $mail->AltBody = $vars['message'];

            return $mail->send();
            
        } catch (\Exception $e) {
        
            $this->_logger->info("Unable to send email. - " . $e->getMessage());
            return false;

        }		
		
    }
}