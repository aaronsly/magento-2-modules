<?php
/**
 * Copyright © Essential E-Commerce Ltd, All rights reserved.
 */

namespace Essential\OrderExporter\Logger;

/**
 * Essential Order Exporter Logger
 * @version 1.0.0
 */

class Logger extends \Monolog\Logger
{
	
	/**
     * Helper instance
     * @var \Essential\OrderExporter\Helper\Data
     */
	protected $_helper;
	
	/**
     * Logging Enabled
     * @var bool
     */
	protected $_loggingEnabled;
	
	public function __construct(
		 $name,		 
		 array $handlers = array(),		 
		 \Essential\OrderExporter\Helper\Data $helper		 
		 )
	 {
		 parent::__construct($name,$handlers);
		 $this->_helper = $helper;
		 $this->_loggingEnabled = $this->_helper->loggingEnabled();
	 }
	
	/*
	* Write info message to logs
	*
	* @var $message string
	* @var $id int, string
	* @var $data array
	* @var $e object
	*
	*/
	public function writeInfo($message, $id = null, $data = null, $e = null) {
		
		if ($this->_loggingEnabled) {
			
			$string = $this->commentString($message, $id, $data, $e);		
			$this->info($string);	
			
		}
		
	}
	
	/*
	* Write error message to logs
	*
	* @var $message string
	* @var $id int, string
	* @var $data array
	* @var $e object
	*
	*/
	public function writeError($message, $id = null, $data = null, $e = null) {
		
		if ($this->_loggingEnabled) {	
			
			$string = $this->commentString($message, $id, $data, $e);		
			$this->error($string);
			
		}
		
	}
	
	/*
	* Build log comment string
	*
	* @var $message string
	* @var $id int, string
	* @var $data array
	* @var $e object
	*
	* return string
	*
	*/
	private function commentString($message, $id, $data, $e) {
		
		$string = $message;
			
		if(!is_null($id))
			$string .= ", Order Id: $id";

		if (!is_null($data) && $this->_helper->verboseLogging())
			$string .= ", Data: " . json_encode($data);

		if (!is_null($e))
			$string .= ", Error Message: " . $e->getMessage();
		
		return $string;
	}
}
