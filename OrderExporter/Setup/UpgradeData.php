<?php
/**
 * Copyright © Essential E-Commerce Ltd, All rights reserved.
 */

namespace Essential\OrderExporter\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Sales\Setup\SalesSetupFactory;
use Magento\Quote\Setup\QuoteSetupFactory;

class UpgradeData implements UpgradeDataInterface {
	
	/**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $eavSetupFactory;
 
    /**
     * @var QuoteSetupFactory
     */
    private $quoteSetupFactory;
 
    /**
     * @var SalesSetup
     */
    private $salesSetupFactory;
 
    /**
     * InstallData constructor.
     * @param EavSetupFactory $eavSetupFactory
     * @param QuoteSetupFactory $quoteSetupFactory
	 * @param SalesSetupFactory $salesSetupFactory
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        QuoteSetupFactory $quoteSetupFactory,
        SalesSetupFactory $salesSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->quoteSetupFactory = $quoteSetupFactory;
        $this->salesSetupFactory = $salesSetupFactory;
    }
 
    public function upgrade( ModuleDataSetupInterface $setup, ModuleContextInterface $context ) { 
		
		$setup->startSetup();
 
        if ( version_compare( $context->getVersion(), '0.0.2', '<' ) ) {
			
			// Update product attributes to include west rads.
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
			$attributeSetId = $eavSetup->getDefaultAttributeSetId('catalog_product');
			
						
			$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY, 'supplier');
			$eavSetup->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
				'supplier',
				[
					'type'                    => 'text',
					'label'                   => 'Supplier',
					'input'                   => 'text',
					'global'                  => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
					'visible'                 => true,
					'required'                => false,
					'user_defined'            => true,
					'default'                 => '',
					'searchable'              => false,
					'filterable'              => false,
					'comparable'              => false,
					'visible_on_front'        => false,
					'used_in_product_listing' => false,
					'unique'                  => false,
					'note'					  => 'For Distinctive Wholesale Ltd products enter "distinctive wholesale", "distinctive wholesale ltd" or "distinctive". For West Radiators products enter "west" or "west radiators"',
				]
			);

			// Add to default attribute set		
			$eavSetup->addAttributeToSet(
				'catalog_product',
				$attributeSetId,
				'General',
				'supplier'
			);
			
			$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY, 'supplier_part_number');
			$eavSetup->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
				'supplier_part_number',
				[
					'type'                    => 'text',
					'label'                   => 'Supplier Part Number',
					'input'                   => 'text',
					'global'                  => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
					'visible'                 => true,
					'required'                => false,
					'user_defined'            => true,
					'default'                 => '',
					'searchable'              => false,
					'filterable'              => false,
					'comparable'              => false,
					'visible_on_front'        => false,
					'used_in_product_listing' => false,
					'unique'                  => false, 
					'note'					  => 'Enter the suppliers part number. For bundled products seperate each part number with "##" for example "code1##code2" - required to export to Distinctive Wholesale Ltd and update stock levels. Also required to export to West Radiators.'
				]
			);		

			$eavSetup->addAttributeToSet(
				'catalog_product',
				$attributeSetId,
				'General',
				'supplier_part_number'
			);

			// Add attributes to quote and order items tables
			$quoteSetup = $this->quoteSetupFactory->create(['setup' => $setup]);        
			$salesSetup = $this->salesSetupFactory->create(['setup' => $setup]);

			$attributeOptions = [
				'type'     => Table::TYPE_TEXT,
				'visible'  => true,
				'required' => false
			];
			$quoteSetup->addAttribute('quote_item', 'supplier', $attributeOptions);
			$quoteSetup->addAttribute('quote_item', 'supplier_part_number', $attributeOptions);
			$salesSetup->addAttribute('order_item', 'supplier', $attributeOptions);
			$salesSetup->addAttribute('order_item', 'supplier_part_number', $attributeOptions);
		}
		
		if (version_compare($context->getVersion(), '0.0.3', '<')) {
            $this->addTowelRads($setup);
        }
		
		$setup->endSetup();
	}
	
	public function addTowelRads(ModuleDataSetupInterface $setup) {
		// Edit supplier attribute
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->updateAttribute(
            \Magento\Catalog\Model\product::ENTITY,
            'supplier',
            'note',
            'For Distinctive Wholesale Ltd products enter "distinctive wholesale", "distinctive wholesale ltd" or "distinctive". For West Radiators products enter "west" or "west radiators". For Towelrads products enter "towelrads"'
        );
        // Edit supplier_part_number attribute
        $eavSetup->updateAttribute(
            \Magento\Catalog\Model\product::ENTITY,
            'supplier_part_number',
            'note',
            'Enter the suppliers part number. For bundled products seperate each part number with "##" for example "code1##code2" - required for stock feeds and order exports to work.'
        );
	}
	
}