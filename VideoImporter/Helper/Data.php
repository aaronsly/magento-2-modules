<?php
/**
 * Copyright © Essential E-Commerce Ltd, All rights reserved.
 */

namespace Essential\VideoImporter\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

/**
 * Essential Order Exporter Helper
 * @version 1.0.0
 */

class Data extends AbstractHelper
{

	
	/**
	* Get config value
	*
	* @var $field string
	* @var $storeId int
	*
	* return string
	*/
	public function getConfigValue($field, $storeId = null)
	{
		return $this->scopeConfig->getValue(
			$field, ScopeInterface::SCOPE_STORE, $storeId
		);
	}

}
