var config = {
    map: {
        '*': {
            minicart_open: 'Essential_MinicartOpen/js/view/minicartopen'
        }
    },
    shim: {
        minicart_open: {
            deps: [
                'jquery'
            ]
        }
    }
};