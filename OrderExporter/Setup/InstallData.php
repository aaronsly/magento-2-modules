<?php
/**
 * Copyright © Essential E-Commerce Ltd, All rights reserved.
 */

namespace Essential\OrderExporter\Setup;
 
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Sales\Setup\SalesSetupFactory;
use Magento\Quote\Setup\QuoteSetupFactory;
 

class InstallData implements InstallDataInterface
{
    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $eavSetupFactory;
 
    /**
     * @var QuoteSetupFactory
     */
    private $quoteSetupFactory;
 
    /**
     * @var SalesSetup
     */
    private $salesSetupFactory;
 
    /**
     * InstallData constructor.
     * @param EavSetupFactory $eavSetupFactory
     * @param QuoteSetupFactory $quoteSetupFactory
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        QuoteSetupFactory $quoteSetupFactory,
        SalesSetupFactory $salesSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->quoteSetupFactory = $quoteSetupFactory;
        $this->salesSetupFactory = $salesSetupFactory;
    }
 
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
		$attributeSetId = $eavSetup->getDefaultAttributeSetId('catalog_product');
		
        /**
         * Add attributes to the eav/attribute
         */
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'supplier',
            [
                'type'                    => 'text',
                'label'                   => 'Supplier',
                'input'                   => 'text',
                'global'                  => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible'                 => true,
                'required'                => false,
                'user_defined'            => true,
                'default'                 => '',
                'searchable'              => false,
                'filterable'              => false,
                'comparable'              => false,
                'visible_on_front'        => false,
                'used_in_product_listing' => false,
                'unique'                  => false,
				'note'					  => 'Enter one of the following if the product is a Distinctive Wholesale Ltd product. "distinctive wholesale", "distinctive wholesale ltd" or "distinctive" - required to export to Distinctive Wholesale Ltd and update stock levels.',
            ]
        );
		
		// Add to default attribute set		
        $eavSetup->addAttributeToSet(
            'catalog_product',
            $attributeSetId,
            'General',
            'supplier'
        );
		
		$eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'supplier_part_number',
            [
                'type'                    => 'text',
                'label'                   => 'Supplier Part Number',
                'input'                   => 'text',
                'global'                  => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible'                 => true,
                'required'                => false,
                'user_defined'            => true,
                'default'                 => '',
                'searchable'              => false,
                'filterable'              => false,
                'comparable'              => false,
                'visible_on_front'        => false,
                'used_in_product_listing' => false,
                'unique'                  => false, 
				'note'					  => 'Enter the suppliers part number. For bundled products seperate each part number with "##" for example "code1##code2" - required to export to Distinctive Wholesale Ltd and update stock levels.'
            ]
        );		
		
        $eavSetup->addAttributeToSet(
            'catalog_product',
            $attributeSetId,
            'General',
            'supplier_part_number'
        );
 	
		// Add attributes to quote and order items tables
        $quoteSetup = $this->quoteSetupFactory->create(['setup' => $setup]);        
        $salesSetup = $this->salesSetupFactory->create(['setup' => $setup]);
		
        $attributeOptions = [
            'type'     => Table::TYPE_TEXT,
            'visible'  => true,
            'required' => false
        ];
        $quoteSetup->addAttribute('quote_item', 'supplier', $attributeOptions);
        $quoteSetup->addAttribute('quote_item', 'supplier_part_number', $attributeOptions);
        $salesSetup->addAttribute('order_item', 'supplier', $attributeOptions);
        $salesSetup->addAttribute('order_item', 'supplier_part_number', $attributeOptions);
    }
}