<?php
/**
 * Copyright © Essential E-Commerce Ltd, All rights reserved.
 */

namespace Essential\OrderExporter\Controller\Index;

/**
 * Used to get West Radiators stock feed .csv file from stockfeeds@traderadiators.com mailbox
 * @version 1.0.0
 */

class Westfeed extends \Magento\Framework\App\Action\Action
{
	/**
     * Logging instance
     * @var \Essential\OrderExporter\Logger\Logger
     */
    protected $_logger;
	
	/**
     * Helper instance
     * @var \Essential\OrderExporter\Helper\Data
     */
	protected $_helper;
	
	/**
     * Mailer instance
     * @var \Essential\OrderExporter\Helper\Email
     */
	protected $_mailer;	
	
	/**
     * Dir instance
     * @var \Magento\Framework\Filesystem\DirectoryList
     */
	protected $_dir;	
	
	/**
     * Path to stock feed csv file
     * @var string
     */
    public $_westFilepath;

    /**
     * Error message array
     * @var array()
     */
	protected $_messages = [];
	
	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Essential\OrderExporter\Helper\Data $helperData,
		\Essential\OrderExporter\Logger\Logger $logger,
		\Essential\OrderExporter\Helper\Email $mailer,
        \Magento\Framework\Filesystem\DirectoryList $dir
	)
	{
		
		parent::__construct($context);		
		$this->_helper = $helperData;
		$this->_logger = $logger;
    	$this->_mailer = $mailer;
    	$this->_dir = $dir;
		$this->_westFilepath = $this->_dir->getRoot() . $this->_helper->getWestStockFeedPath();
		
	}

	public function execute()
	{		
        
        if( $this->_helper->stockFeedEnabled() && $this->_helper->stockExternalCron() == false ) {
            
            $this->getWestCsv();
            $this->sendErrorEmails();
		    
        }

    } // execute end	
    
    public function getWestCsv() {

        $mailbox = new \PhpImap\Mailbox(
            '{imap.gmail.com:993/imap/ssl}INBOX',
            $this->_helper->getStockFeedEmail(),
            $this->_helper->getStockFeedPassword(),
            $this->_dir->getRoot() . '/var/tmp'
        );

        try {

            // Get all stock feed emails from today            
            $mailsIds = $mailbox->searchMailbox( 'SUBJECT "West Radiators Stock Feed" SINCE "' . date('d-M-Y') . '"' );

        } catch( PhpImap\Exceptions\ConnectionException $e ) {

            $this->_logger->writeError( "Unable to connect to stock feed email mailbox.", null, null, $e->getMessage() );
            $this->_messages[] = "Unable to connect to stock feed email mailbox.";
            die();

        }

        // If $mailsIds is empty, no emails could be found
        if( !$mailsIds ) {

            $this->_logger->writeError( "No West stock feed emails found." );
            $this->_messages[] = "No West stock feed emails found.";
            die();

        }

        // Get the latest message
        $mail = $mailbox->getMail( $mailsIds[count( $mailsIds ) - 1] );

        // Save file if email has attachments
        if( $mail->hasAttachments() ) {
            
            $attachments = $mail->getAttachments();
            $attachment = reset( $attachments );
            $filePath = $attachment->filePath;

            if( file_exists( $filePath ) )
                rename( $filePath, $this->_westFilepath );

        } else {

           // log no csv found
           $this->_logger->writeError( "No .csv file attachment found." );
           $this->_messages[] = "No .csv file attachment found.";

        }

    } // getWestCsv() end

    public function sendErrorEmails() {
		
		if ($this->_messages) {

			$html = '';

			foreach( $this->_messages as $message ) {

				$html .= '<h3 style="text-decoration:underline;">West Stock Feed Error(s):</h3>';
                $html .= '<p>' . $message . '</p>';
                
			}

			$vars = ['subject' => "West Stock Feed Error" , 'message' => $html];
			$this->_mailer->sendErrorEmails( $vars, 'stock' );

		}

	}
}
