<?php
/**
 * Copyright © Essential E-Commerce Ltd, All rights reserved.
 */

namespace Essential\OrderExporter\Plugin;

/**
 * Essential Order Exporter - Sets supplier on order item
 * @version 1.0.0
 */

class SupplierAttributeQuoteToOrderItem
{
    public function aroundConvert(
        \Magento\Quote\Model\Quote\Item\ToOrderItem $subject,
        \Closure $proceed,
        \Magento\Quote\Model\Quote\Item\AbstractItem $item,
        $additional = []
    ) {
        /** @var $orderItem \Magento\Sales\Model\Order\Item */
        $orderItem = $proceed($item, $additional);
        $orderItem->setSupplier($item->getSupplier());
        $orderItem->setSupplierPartNumber($item->getSupplierPartNumber());
        return $orderItem;
    }
}