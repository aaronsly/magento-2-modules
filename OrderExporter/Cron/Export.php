<?php
/**
 * Copyright © Essential E-Commerce Ltd, All rights reserved.
 */

namespace Essential\OrderExporter\Cron;

/**
 * Used to export Distinctive Wholesale Products
 * @version 1.0.0
 */

class Export
{
	/**
     * Logging instance
     * @var \Essential\OrderExporter\Logger\Logger
     */
    protected $_logger;
	
	/**
     * Helper instance
     * @var \Essential\OrderExporter\Helper\Data
     */
	protected $_helper;
	
	/**
     * Exported Orders instance
     * @var \Essential\OrderExporter\Model\Orders
     */
	protected $_orders;
	
	/**
     * Magento Orders instance
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
	protected $_orderFactory;
	
	/**
     * Curl instance
     * @var \Magento\Framework\HTTP\Client\Curl
     */
	protected $_curl;
	
	/**
     * Mailer instance
     * @var \Essential\OrderExporter\Helper\Email
     */
	protected $_mailer;
	
	/**
     * Directory List instance
     * @var \Magento\Framework\Filesystem\DirectoryList
     */
	protected $_dir;
	
	/**
     * Live or Sandbox url set in admin
     * @var string
     */
	protected $_url;
	
	/**
     * Email Addresses for West export
     * @var string
     */
	protected $_westEmails;

	/**
     * Email Addresses for Towelrads export
     * @var string
     */
	protected $_towelradsEmails;
	
	/**
     * All order statuses
     * @var array
     */
	protected $_orderStatuses;
	
	/**
     * Error messages
     * @var array()
     */
	protected $_messages = [];
	
	
	public function __construct(
		\Essential\OrderExporter\Helper\Data $helperData,
		\Essential\OrderExporter\Logger\Logger $logger,
		\Essential\OrderExporter\Helper\Email $mailer,
		\Essential\OrderExporter\Model\Orders $orders,
		\Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
		\Magento\Framework\HTTP\Client\Curl $curl,
		\Magento\Framework\Filesystem\DirectoryList $dir)
	{
			
		$this->_helper = $helperData;
		$this->_logger = $logger;
		$this->_orders = $orders;
    	$this->_orderFactory = $orderCollectionFactory;
    	$this->_curl = $curl;
    	$this->_mailer = $mailer;
		$this->_dir = $dir;
    	$this->_url = $this->_helper->sandboxEnabled() ? $this->_helper->getSandboxUrl() : $this->_helper->getLiveUrl();
		$this->_westEmails = $this->_helper->sandboxEnabled() ? $this->_helper->getSandboxEmails() : $this->_helper->getWestEmails();
		$this->_towelradsEmails = $this->_helper->sandboxEnabled() ? $this->_helper->getSandboxEmails() : $this->_helper->getTowelradsEmails();
		$this->_orderStatuses = $this->_orders->getAllStatus();
		
	}

	public function execute()
	{
		
		if($this->_helper->exportsEnabled() && $this->_helper->orderExternalCron() == false) {
			
			$flag = $this->_dir->getPath('tmp')."/order-export.flag";
		
			if (file_exists($flag)) {
				$this->_logger->writeError("Exporter is already running.");
				exit();
			}			

			file_put_contents($flag,"");
			
			try {				
				// load all orders with selected order statuses
				$orderCollection = $this->_orderFactory->create()->addAttributeToSelect('*');		
				$orders = $orderCollection
						->addFieldToFilter('status', array('in' => $this->_helper->getExportStatuses()))
						->setOrder('created_at', 'ASC');				
			} catch (Exception $e) {				
				$this->_messages[$id][] = [	'message' => "Unable to load order data.",'heading' => "Unable To Export Order"];				
				$this->_logger->writeError("Unable to load order data", null, null ,$e->getMessage());				
			}
			
			if ($orders->getSize()) {
				
				foreach ($orders as $order) {				
					
					$id = $order->getIncrementId();
					$loggedOrderInfo = $this->_orders->getOrderById($id);
					
					// check if order is in the essential_order_exporter db
					if ($loggedOrderInfo == 'error') {						
						$this->_messages[$id][] = [	'message' => "Unable to connect to order exporter database.", 'heading' => "Unable To Export Order"];
						continue;						
					}

					if ( $loggedOrderInfo ) {
						
						// ignore non DW, Towelrads and West orders and exported orders
						if ((bool) $loggedOrderInfo['ignore'] || (bool) $loggedOrderInfo['exported'])
							continue; 

						$tableId = $loggedOrderInfo['id'];
						
						// Reimport order
						if ( $loggedOrderInfo['export_attempts'] <= 3 ) {
							
							// get and submit order data to Distinctive Wholesale's portal
							$dataToSend = $this->buildOrder($order);

							// reimport distinctive orders
							if ( $loggedOrderInfo['distinctive_exported'] == false ) {								
								if ($dataToSend['result'] && $dataToSend['dw_order'] != false)									
									$dwSuccess = $this->exportDistinctiveOrder($order, $id, $dataToSend, $loggedOrderInfo['export_attempts']);							
							}							
							
							// reimport west orders
							if ( $loggedOrderInfo['west_exported'] == false ) {
								if ($dataToSend['result'] && $dataToSend['west_order'] != false)								
									$westSuccess = $this->exportWestOrder($order, $id, $dataToSend,$loggedOrderInfo['export_attempts']);
							}

							// reimport towelrads orders
							if ( $loggedOrderInfo['towelrads_exported'] == false ) {
								if ($dataToSend['result'] && $dataToSend['towelrads_order'] != false)
									$towelradsSuccess = $this->exportTowelradsOrder($order, $id, $dataToSend);
							}
							
							// order only contains distinctive products - export successful
							if ($dataToSend['dw_order'] && $dataToSend['west_order'] == false && $dataToSend['towelrads_order'] == false) {
								if (isset($dwSuccess) && $dwSuccess) {
									$order->setStatus('ec_exported_dw')->addStatusToHistory($order->getStatus())->save();
									$this->_orders->saveOrder($tableId, $id, null, true, null, ['distinctive', true]);
								} elseif(!$this->_helper->distinctiveExportsEnabled()) {
									$this->_orders->saveOrder(null, $id, null, true, null, ['distinctive', true]);
								}						
							}

							// order only contains towelrads products - export successful
							if ($dataToSend['towelrads_order'] && $dataToSend['west_order'] == false && $dataToSend['dw_order'] == false) {
								if (isset($towelradsSuccess) && $towelradsSuccess == true) {
									$order->setStatus('ec_exported_tr')->addStatusToHistory($order->getStatus())->save();
									$this->_orders->saveOrder($tableId, $id, null, true, null, ['towelrads', true]);
								} elseif(!$this->_helper->towelradsExportsEnabled()) {
									$this->_orders->saveOrder(null, $id, null, true, null, ['towelrads', true]);
								}							
							}

							// order only contains west products - export successful
							if ($dataToSend['west_order'] && $dataToSend['dw_order'] == false && $dataToSend['towelrads_order'] == false) {
								if (isset($westSuccess) && $westSuccess) {
									$order->setStatus('ec_exported_west')->addStatusToHistory($order->getStatus())->save();
									$this->_orders->saveOrder($tableId, $id, null, true, null,['west', true] );
								} elseif( !$this->_helper->westExportsEnabled() ) {
									$this->_orders->saveOrder(null, $id, null, true, null, ['west', true]);
								}
							}

							// Order contains all suppliers - exported successfully
							if ($dataToSend['dw_order'] && $dataToSend['west_order'] && $dataToSend['towelrads_order']) {

								if ( (isset($dwSuccess) && $dwSuccess == true) && (isset($westSuccess) && $westSuccess == true) && (isset($towelradsSuccess) && $towelradsSuccess == true) ) {								
									$order->setStatus('ec_exported')->addStatusToHistory($order->getStatus())->save();
									$this->_orders->saveOrder(null, $id, 1, true);
								}

								if (!$this->_helper->distinctiveExportsEnabled())
									$this->_orders->saveOrder(null, $id, null, true, null, ['distinctive', true]);							

								if( !$this->_helper->towelradsExportsEnabled() )
									$this->_orders->saveOrder(null, $id, null, true, null, ['towelrads', true]);

								if( !$this->_helper->westExportsEnabled() )
									$this->_orders->saveOrder(null, $id, null, true, null, ['west', true]);
								
							}
							
							// Order has Distinctive and west products - both successful
							if ($dataToSend['dw_order'] && $dataToSend['west_order'] && $dataToSend['towelrads_order'] == false) {

								if ( (isset($dwSuccess) && $dwSuccess == true) && (isset($westSuccess) && $westSuccess == true) ) {								
									$order->setStatus('ec_exported')->addStatusToHistory($order->getStatus())->save();
									$this->_orders->saveOrder(null, $id, 1, true);
								}

								if (!$this->_helper->distinctiveExportsEnabled())
									$this->_orders->saveOrder(null, $id, null, true, null, ['distinctive', true]);

								if( !$this->_helper->westExportsEnabled() )
									$this->_orders->saveOrder(null, $id, null, true, null, ['west', true]);
							}

							// Order has Distinctive and towelrads products - both successful
							if ($dataToSend['dw_order'] && $dataToSend['towelrads_order'] && $dataToSend['west_order'] == false) {

								if ( (isset($dwSuccess) && $dwSuccess == true) && (isset($towelradsSuccess) && $towelradsSuccess == true) ) {								
									$order->setStatus('ec_exported')->addStatusToHistory($order->getStatus())->save();
									$this->_orders->saveOrder(null, $id, 1, true);
								}

								if (!$this->_helper->distinctiveExportsEnabled())
									$this->_orders->saveOrder(null, $id, null, true, null, ['distinctive', true]);

								if( !$this->_helper->towelradsExportsEnabled() )
									$this->_orders->saveOrder(null, $id, null, true, null, ['towelrads', true]);
							}

							// Order has West and towelrads products - both successful
							if ($dataToSend['west_order'] && $dataToSend['towelrads_order'] && $dataToSend['dw_order'] == false) {

								if ( (isset($westSuccess) && $westSuccess == true) && (isset($towelradsSuccess) && $towelradsSuccess == true) ) {								
									$order->setStatus('ec_exported')->addStatusToHistory($order->getStatus())->save();
									$this->_orders->saveOrder(null, $id, 1, true);
								}

								if (!$this->_helper->towelradsExportsEnabled())
									$this->_orders->saveOrder(null, $id, null, true, null, ['towelrads', true]);

								if( !$this->_helper->westExportsEnabled() )
									$this->_orders->saveOrder(null, $id, null, true, null, ['west', true]);
							}
							
						} else {
							// save export failure result to db - to many attempts
							try {								
								$order->setStatus('ec_export_failed')->addStatusToHistory($order->getStatus())->save();								
								$saved = $this->_orders->saveOrder($tableId, $id, $loggedOrderInfo['export_attempts'], false, true);	

								if($saved == false) {								
									$this->_messages[$id][] = ['message' => "Unable to save results to the order exporter database.",'heading' => "Error Saving Data"];
								}
								
							} catch (Exception $e) {								
								$this->_logger->writeError("Unable to update order status", $id, null, $e->getMessage());
								$this->_messages[$id][] = ['message' => "Unable to set order status.",'heading' => "Order Status Error"];								
							}
							
							$this->_logger->writeError("Unable to export order to many attempts", $id);
							$this->_messages[$id][] = ['message' => "Order export has failed. To many failed export attempts please process the order manually and investigate the issue.", 'heading' => "Order Export Failed"];
							
						}	
						
					} else {						
						
						// Export order						
						$dataToSend = $this->buildOrder($order);

						// Export to dw
						if ($dataToSend['result'] && $dataToSend['dw_order'] != false)						
							$dwSuccess = $this->exportDistinctiveOrder($order, $id, $dataToSend);

						// Send CSV to towelrads
						if ($dataToSend['result'] && $dataToSend['towelrads_order'] != false)
							$towelradsSuccess = $this->exportTowelradsOrder($order, $id, $dataToSend);
						
						// Send CSV to west
						if ($dataToSend['result'] && $dataToSend['west_order'] != false)
							$westSuccess = $this->exportWestOrder($order, $id, $dataToSend);
						
						// order only contains distinctive products - export successful
						if ($dataToSend['dw_order'] && $dataToSend['west_order'] == false && $dataToSend['towelrads_order'] == false) {
							if (isset($dwSuccess) && $dwSuccess == true){
								$order->setStatus('ec_exported_dw')->addStatusToHistory($order->getStatus())->save();
								$this->_orders->saveOrder(null, $id, 1, true);
							} elseif(!$this->_helper->distinctiveExportsEnabled()) {
								$this->_orders->saveOrder(null, $id, null, true, null, ['distinctive', true]);
							}							
						}

						// order only contains towelrads products - export successful
						if ($dataToSend['towelrads_order'] && $dataToSend['west_order'] == false && $dataToSend['dw_order'] == false) {
							if (isset($towelradsSuccess) && $towelradsSuccess == true){
								$order->setStatus('ec_exported_tr')->addStatusToHistory($order->getStatus())->save();
								$this->_orders->saveOrder(null, $id, 1, true);
							} elseif(!$this->_helper->towelradsExportsEnabled()) {
								$this->_orders->saveOrder(null, $id, null, true, null, ['towelrads', true]);
							}							
						}
						
						// order only contains west products - export successful
						if ($dataToSend['west_order'] && $dataToSend['dw_order'] == false && $dataToSend['towelrads_order'] == false) {
							if (isset($westSuccess) && $westSuccess) {
								$order->setStatus('ec_exported_west')->addStatusToHistory($order->getStatus())->save();
								$this->_orders->saveOrder(null, $id, 1, true);
							} elseif( !$this->_helper->westExportsEnabled() ) {
								$this->_orders->saveOrder(null, $id, null, true, null, ['west', true]);
							}								
						}
						
						// Order contains all suppliers - exported successfully
						if ($dataToSend['dw_order'] && $dataToSend['west_order'] && $dataToSend['towelrads_order']) {

							if ( (isset($dwSuccess) && $dwSuccess == true) && (isset($westSuccess) && $westSuccess == true) && (isset($towelradsSuccess) && $towelradsSuccess == true) ) {								
								$order->setStatus('ec_exported')->addStatusToHistory($order->getStatus())->save();
								$this->_orders->saveOrder(null, $id, 1, true);
							}

							if (!$this->_helper->distinctiveExportsEnabled())
								$this->_orders->saveOrder(null, $id, null, true, null, ['distinctive', true]);							

							if( !$this->_helper->towelradsExportsEnabled() )
								$this->_orders->saveOrder(null, $id, null, true, null, ['towelrads', true]);

							if( !$this->_helper->westExportsEnabled() )
								$this->_orders->saveOrder(null, $id, null, true, null, ['west', true]);
							
						}
						
						// Order has Distinctive and west products - both successful
						if ($dataToSend['dw_order'] && $dataToSend['west_order'] && $dataToSend['towelrads_order'] == false) {

							if ( (isset($dwSuccess) && $dwSuccess == true) && (isset($westSuccess) && $westSuccess == true) ) {								
								$order->setStatus('ec_exported')->addStatusToHistory($order->getStatus())->save();
								$this->_orders->saveOrder(null, $id, 1, true);
							}

							if (!$this->_helper->distinctiveExportsEnabled())
								$this->_orders->saveOrder(null, $id, null, true, null, ['distinctive', true]);

							if( !$this->_helper->westExportsEnabled() )
								$this->_orders->saveOrder(null, $id, null, true, null, ['west', true]);
						}

						// Order has Distinctive and towelrads products - both successful
						if ($dataToSend['dw_order'] && $dataToSend['towelrads_order'] && $dataToSend['west_order'] == false) {

							if ( (isset($dwSuccess) && $dwSuccess == true) && (isset($towelradsSuccess) && $towelradsSuccess == true) ) {								
								$order->setStatus('ec_exported')->addStatusToHistory($order->getStatus())->save();
								$this->_orders->saveOrder(null, $id, 1, true);
							}

							if (!$this->_helper->distinctiveExportsEnabled())
								$this->_orders->saveOrder(null, $id, null, true, null, ['distinctive', true]);

							if( !$this->_helper->towelradsExportsEnabled() )
								$this->_orders->saveOrder(null, $id, null, true, null, ['towelrads', true]);
						}

						// Order has West and towelrads products - both successful
						if ($dataToSend['west_order'] && $dataToSend['towelrads_order'] && $dataToSend['dw_order'] == false) {

							if ( (isset($westSuccess) && $westSuccess == true) && (isset($towelradsSuccess) && $towelradsSuccess == true) ) {								
								$order->setStatus('ec_exported')->addStatusToHistory($order->getStatus())->save();
								$this->_orders->saveOrder(null, $id, 1, true);
							}

							if (!$this->_helper->towelradsExportsEnabled())
								$this->_orders->saveOrder(null, $id, null, true, null, ['towelrads', true]);

							if( !$this->_helper->westExportsEnabled() )
								$this->_orders->saveOrder(null, $id, null, true, null, ['west', true]);
						}						
						
						// contains no export products save to db and set ignore to true
						if ($dataToSend['result'] == true && $dataToSend['dw_order'] == false && $dataToSend['west_order'] == false) {
							$this->_orders->saveOrder(null, $id, 1, false, true);						
							$this->_logger->writeInfo("No Distinctive, Towelrads or West products to export", $id, $dataToSend);							
						}
						
					}
					
				}
				
			} else {				
				$this->_logger->writeInfo("No orders to export.");				
			}
			
			// send error emails
			if (!empty($this->_messages)) {
				
				foreach($this->_messages as $id => $messages) {	
					
					$vars = ['mageOrderId' => $id, 'subject' => "Order Export Errors - Order: $id" , 'message' => ''];
					
					foreach($messages as $message) {
						$vars['message'] .= '<p><span style="font-weight:bold;">'. $message['heading'] .':</span> '. $message['message'] .'</p>';
					}	
					
					$this->_mailer->sendErrorEmails($vars);
				}
				
			}
			
			unlink($flag);
			
		}	

	} // execute end
	
	/*
	* Gather order product and information to submit to Distinctive Wholesale and West Radiators
	*
	* $order object 
	*
	* return array
	*/
	public function buildOrder($order) {
		
		$id = $order->getIncrementId();
		$dwData = $westData = $towelradsData = false;
		
		try {
			
			$shippingAddress = $order->getShippingAddress()->getData();
			$billingAddress = $order->getBillingAddress()->getData();
			$orderItems = $order->getAllItems();
			
		} catch (Exception $e) {
			
			$this->_logger->writeError("Order export unsuccessful, Unable to load customer or product details", $order->getIncrementId());
			$this->_messages[$id][] = ['message' => "Unable to load customer or product details",'heading' => 'Load Data Error'];
			
			// Problem loading order data
			return ['result' => false, 'dw_order' => false, 'west_order'=> false, 'towelrads_order' => false];
			
		}	
		
		// pull distinctive product from order by supplier attribute
		$i = 1;
		$products = [];
		foreach ($orderItems as $item) {
			
			if ($item->getProductType() == 'configurable')
				continue;
			
			$supplier = strtolower(trim($item->getSupplier()));
			
			if (in_array($supplier, ['distinctive wholesale', 'distinctive wholesale ltd', 'distinctive', 'west', 'west radiators', 'towelrads'])) {
				
				$supplierPartNumber = str_replace(" ", "", $item->getSupplierPartNumber());

				$qty = $item->getQtyOrdered();
				
				if (!empty($supplierPartNumber)) {
					
					//$sKey = in_array($supplier, ['distinctive wholesale', 'distinctive wholesale ltd', 'distinctive']) ? 'distinctive' : 'west';

					switch(true) {
						case in_array($supplier, ['distinctive wholesale', 'distinctive wholesale ltd', 'distinctive']):
							$sKey = 'distinctive';
							break;
						case in_array($supplier, ['west', 'west radiators']):
							$sKey = 'west';
							break;
						case $supplier == 'towelrads':
							$sKey = 'towelrads';
							break;
					}

					// Split bundled products					
					if(strpos($supplierPartNumber, '##') !== false) {
						
						$parts = explode("##", $supplierPartNumber);
						
						foreach($parts as $part) {							
							$products[$sKey]['product_'.$i] = [
								'varCode'	=> $part,
								'qty'		=> number_format($qty,0)
							];
							$i++;							
						}
						
					} else {						
						$products[$sKey]['product_'.$i] = [
							'varCode'	=> $supplierPartNumber,
							'qty'		=> number_format($qty,0)
						];
						$i++;						
					}					
					
				} else {		
					$this->_logger->writeError("Order export unsuccessful, ". $item->getSku() . ' has no supplier part number to export.', $order->getIncrementId());
					$this->_messages[$id][] = ['message' => $item->getSku() . ' has no supplier part number to export.', 'heading' => 'Product Error'];
					
					return ['result' => false, 'dw_order' => false, 'west_order'=> false, 'towelrads_order' => false, 'product_message' => $item->getSku() . ' has no supplier part number to export.'];					
				}				
				
			}
			
		}		
		
		// Order has distinctive products
		if ( !empty($products['distinctive']) ) {
			
			$dwData = [
				'data' => [
					'custRef' 	   => "Trade Order " . substr($billingAddress['firstname'], 0, 1). " " . $billingAddress['lastname'],
					'emailSubject' => 'Trade Order ' . $billingAddress['lastname'],
					'custDetails'  => [
						'name' 		  => trim($billingAddress['firstname']) . " " . trim($billingAddress['lastname']),
						'email' 	  => $billingAddress['email'],
						'phone' 	  => $shippingAddress['telephone'],
						'address1' 	  => $shippingAddress['street'],
						'address2' 	  => '',
						'town' 		  => $shippingAddress['city'],
						'county' 	  => $shippingAddress['region'],
						'postcode' 	  => $shippingAddress['postcode'],
					],							
					'countryCode'  => $shippingAddress['country_id'],
					'takenBy' 	   => '',
					'owId' 		   => 1011,
					'products'	   => $products['distinctive']
				]
			];
			
			$result = true;
			
		}
		
		// Order has west products
		if( !empty($products['west']) ) {
			
			$result = true;
			$address = $this->splitAddress($shippingAddress['street']);
			$westData = [
				'data' => [
					'custRef' 	   => "Trade Order " . substr($billingAddress['firstname'], 0, 1). " " . $billingAddress['lastname'],					
					'custDetails'  => [
						'name' 		  => trim($billingAddress['firstname']) . " " . trim($billingAddress['lastname']),
						'email' 	  => $billingAddress['email'],
						'phone' 	  => $shippingAddress['telephone'],
						'address1' 	  => isset($address['address1']) ? $address['address1'] : $shippingAddress['street'],
						'address2' 	  => isset($address['address2']) ? $address['address2'] : '',
						'town' 		  => $shippingAddress['city'],
						'county' 	  => $shippingAddress['region'],
						'postcode' 	  => $shippingAddress['postcode'],
					],
					'products'	   => $products['west']
				]
			];
		}

		// Order has towelrads products
		if( !empty($products['towelrads']) ) {
			
			$result = true;
			$address = $this->splitAddress($shippingAddress['street']);
			$towelradsData = [
				'data' => [
					'custRef' 	   => "Trade Order " . substr($billingAddress['firstname'], 0, 1). " " . $billingAddress['lastname'],				
					'custDetails'  => [
						'name' 		  => trim($billingAddress['firstname']) . " " . trim($billingAddress['lastname']),
						'email' 	  => $billingAddress['email'],
						'phone' 	  => $shippingAddress['telephone'],
						'address1' 	  => isset($address['address1']) ? $address['address1'] : $shippingAddress['street'],
						'address2' 	  => isset($address['address2']) ? $address['address2'] : '',
						'town' 		  => $shippingAddress['city'],
						'county' 	  => $shippingAddress['region'],
						'postcode' 	  => $shippingAddress['postcode'],
					],
					'products'	   => $products['towelrads']
				]
			];
		}
		
		if (empty($products['distinctive']) && empty($products['west']) && empty($products['towelrads'])) {
			// successfully loaded data but contains no Distinctive Wholesale or west products
			$result = true;
			$dwData = false;
			$westData = false;
			$towelradsData = false;	
		}
		
		// successfully loaded data and contains Distinctive Wholesale products
		return ['result' => $result, 'dw_order' => $dwData, 'west_order'=> $westData, 'towelrads_order' => $towelradsData ];
		
	} // buildOrder end	
	
	/*
	* Export order to Distinctive Wholesale
	*
	* $order object
	* $id string magento order id
	* $dataToSend array
	* $attempts int
	*
	* return bool
	*/
	public function exportDistinctiveOrder($order, $id, $dataToSend, $attempts = null) {

		if( !$this->_helper->distinctiveExportsEnabled() )
			return false;
		
		$dwSuccess = false;
		$attempts = is_null($attempts) ? 0 : $attempts;
		
		try {								
			$this->_curl->post($this->_url, $dataToSend['dw_order']);	
			$response = json_decode($this->_curl->getBody());								
		} catch (Exception $e){									
			$this->_logger->writeError("Order export unsuccessful, No response from curl request", $id, $dataToSend);								
		}

		if (isset($response) && $response->result) {

			$dwSuccess = true;
			// save successfull export result to db								
			try {									
				$order->addStatusHistoryComment($this->_orderStatuses['ec_exported_dw'])->save();									
				$dwSaved = $this->_orders->saveOrder(null, $id, $attempts + 1, null, null, ['distinctive',true]);

				if($dwSaved == false) {										
					$this->_messages[$id][] = ['message' => "Order has been succesfully exported to Distinctive Wholesale but unable to save results to the order exporter database.",'heading' => "Error Saving Data"];										
				}

			} catch (Exception $e) {									
				$this->_logger->writeError("Order has been succesfully exported to Distinctive Wholesale but unable to update order status", $id, null, $e->getMessage());
				$this->_messages[$id][] = ['message' => "Unable to set order status.",'heading' => "Order Status Error"];							
			}								

			$this->_logger->writeInfo("Order exported successfully", $id, $dataToSend);

		} else {
			// save unsuccessfull result to db and update status to be attempted again on next cron
			try {									
				$order->setStatus('ec_retrying')->addStatusToHistory($order->getStatus())->save();	
				
				$this->_orders->saveOrder(null, $id, $attempts + 1, false, null, ['distinctive',false]);									

			} catch (Exception $e) {									
				$this->_logger->writeError("Order has not been exported and was unable to update order status or save to exported table", $id, null, $e->getMessage());
				$this->_messages[$id][] = ['message' => "Unable to set order status or save data.",'heading' => "Order Status Error"];									
			}

			$this->_logger->writeError("Order export unsuccessful", $id, ['data' => $dataToSend, 'response' => $response]);
			$this->_messages[$id][] = ['message' => "Unable to export order to Distinctive Wholesale at this time. The export will be attempted again the next time the export script runs.", 'heading' => "Unable To Export"];

		}
		
		return $dwSuccess;
	}
	
	/*
	* Create csv file to export to West Raditors
	*
	* $data array sorted West product data
	*
	* return bool
	*/
	public function createWestCsv($data) {
		
		$headings = ['Name','Surname','Address1','Address2','Address3','Address4','Post Code','Telephone Number','Email','WEST SKU','QTY'];
		$outputFile = $this->_dir->getPath('tmp') . "/" . str_replace(" ", "_", $data['custRef']) . "_" .date('Ymd_His').".csv";
		$handle = fopen($outputFile, 'w');
		fputcsv($handle, $headings);
		
		foreach ($data['products'] as $product) {
			$name = explode(" ", $data['custDetails']['name']);
			$row = [
				$name[0],
				$name[1],
				$data['custDetails']['address1'],
				$data['custDetails']['address2'],
				$data['custDetails']['town'],
				$data['custDetails']['county'],
				$data['custDetails']['postcode'],
				$data['custDetails']['phone'],
				$data['custDetails']['email'],
				str_replace(" ", "", $product['varCode']),          
				$product['qty'],          
			];
			fputcsv($handle, $row);
		}
		
		return $outputFile;		
	} // createWestCsv end
	
	/*
	* Export order to West Radiators
	*
	* $order object
	* $id string magento order id
	* $dataToSend array
	* $attempts int
	*
	* return bool
	*/
	public function exportWestOrder($order, $id, $dataToSend, $attempts = null) {

		if( !$this->_helper->westExportsEnabled() )
			return false;

		$westSuccess = false;
		$attempts = is_null($attempts) ? 0 : $attempts;
		$csv = $this->createWestCsv($dataToSend['west_order']['data']);	

		if (file_exists($csv)) {

			// Added 16/08/2019 - Aaron vvv
			$westMessage = 'Delivery Address:<br>';
			$westMessage .= $dataToSend['west_order']['data']['custDetails']['name'] . '<br>';
			$westMessage .= $dataToSend['west_order']['data']['custDetails']['address1'] . '<br>';

			if( !empty( $dataToSend['west_order']['data']['custDetails']['address2'] ) )
				$westMessage .= $dataToSend['west_order']['data']['custDetails']['address2'] . '<br>';

			$westMessage .= $dataToSend['west_order']['data']['custDetails']['town'] . '<br>';
			$westMessage .= $dataToSend['west_order']['data']['custDetails']['county'] . '<br>';
			$westMessage .= $dataToSend['west_order']['data']['custDetails']['postcode'] . '<br>';
			$westMessage .= $dataToSend['west_order']['data']['custDetails']['email'] . '<br>';
			$westMessage .= $dataToSend['west_order']['data']['custDetails']['phone'] . '<br><br>';

			foreach( $order->getAllItems() as $item ) {

				if ($item->getProductType() == 'configurable')
					continue;
				
				if ( in_array( strtolower( trim( $item->getSupplier() ) ), ['west', 'west radiators'] ) ) {
					
					$westMessage .= (int) $item->getQtyOrdered() . ' x ' . $item->getSku() . ' - ' . $item->getName() . ' - (Supplier part number(s): ';

					$supplierPartNumber = str_replace(" ", "", $item->getSupplierPartNumber());
					$partNums = [];
					if(strpos($supplierPartNumber, '##') !== false) {
						
						$parts = explode("##", $supplierPartNumber);
						
						foreach( $parts as $part ) {							
							$partNums[] = $part;
						}
						
					} else {
						$partNums[] = $supplierPartNumber;
					}
					
					$westMessage .= implode( ', ', $partNums ).')<br><br>';
				}

			}

			$westVars = [
				'subject' => $dataToSend['west_order']['data']['custRef'],
				// 'message' => "New Trade Radiators Order - " . $dataToSend['west_order']['data']['custRef']
				'message' => $westMessage,
			];
			// Added 16/08/2019 - Aaron ^^^
			
			$emailSent = $this->_mailer->sendEmail($westVars, explode(",", str_replace(" ","",$this->_westEmails)), $csv);

			if($emailSent) {
				$westSuccess = true;
				$order->addStatusHistoryComment($this->_orderStatuses['ec_exported_west'])->save();
				$wSaved = $this->_orders->saveOrder(null, $id, $attempts + 1, null, null, ['west',true]);

				if($wSaved == false) {										
					$this->_messages[$id][] = ['message' => "Order has been exported to West but was unable to save results to the order exporter database.",'heading' => "Error Saving Data"];										
				}

			} else {
				// save unsuccessful attempt
				$order->setStatus('ec_retrying')->addStatusToHistory($order->getStatus())->save();
				$this->_orders->saveOrder(null, $id, $attempts + 1, false, null, ['west',false]);
				$this->_logger->writeError("Order export unsuccessful - Unable to send email.", $id, ['data' => $dataToSend, 'response' => $emailSent]);
				$this->_messages[$id][] = ['message' => "Unable to export order to West at this time. The export will be attempted again the next time the export script runs.", 'heading' => "Unable To Export"];
			}

		} else {
			// save unsuccessful attempt
			$order->setStatus('ec_retrying')->addStatusToHistory($order->getStatus())->save();
			$this->_orders->saveOrder(null, $id, $attempts + 1, false, null, ['west',false]);
			$this->_logger->writeError("Order export unsuccessful - Unable to create csv.", $id, ['data' => $dataToSend, 'response' => $emailSent]);
			$this->_messages[$id][] = ['message' => "Unable to export order to West at this time. The export will be attempted again the next time the export script runs.", 'heading' => "Unable To Export"];
		}

		unlink($csv);
		
		return $westSuccess;
	}

	/*
	* Create csv file to export to Towelrads
	*
	* $data array sorted Towelrads product data
	*
	* return bool
	*/
	public function createTowelradsCsv($data) {
		
		$headings = ['Name','Surname','Address1','Address2','Address3','Address4','Post Code','Telephone Number','Email','Product Code','QTY'];
		$outputFile = $this->_dir->getPath('tmp') . "/" . str_replace(" ", "_", $data['custRef']) . "_TR_" .date('Ymd_His').".csv";
		$handle = fopen($outputFile, 'w');
		fputcsv($handle, $headings);
		
		foreach ($data['products'] as $product) {
			$name = explode(" ", $data['custDetails']['name']);
			$row = [
				$name[0],
				$name[1],
				$data['custDetails']['address1'],
				$data['custDetails']['address2'],
				$data['custDetails']['town'],
				$data['custDetails']['county'],
				$data['custDetails']['postcode'],
				$data['custDetails']['phone'],
				$data['custDetails']['email'],
				str_replace(" ", "", $product['varCode']),          
				$product['qty'],          
			];
			fputcsv($handle, $row);
		}
		
		return $outputFile;
	} // createTowelradsCsv end

	/*
	* Export order to Towelrads
	*
	* $order object
	* $id string magento order id
	* $dataToSend array
	* $attempts int
	*
	* return bool
	*/
	public function exportTowelradsOrder($order, $id, $dataToSend, $attempts = null) {

		if( !$this->_helper->towelradsExportsEnabled() )
			return false;

		$towelradsSuccess = false;
		$attempts = is_null($attempts) ? 0 : $attempts;
		$csv = $this->createTowelradsCsv($dataToSend['towelrads_order']['data']);	

		if (file_exists($csv)) {

			// Added 16/08/2019 - Aaron vvv
			$towelradsMessage = 'Delivery Address:<br>';
			$towelradsMessage .= $dataToSend['towelrads_order']['data']['custDetails']['name'] . '<br>';
			$towelradsMessage .= $dataToSend['towelrads_order']['data']['custDetails']['address1'] . '<br>';

			if( !empty( $dataToSend['towelrads_order']['data']['custDetails']['address2'] ) )
				$towelradsMessage .= $dataToSend['towelrads_order']['data']['custDetails']['address2'] . '<br>';

			$towelradsMessage .= $dataToSend['towelrads_order']['data']['custDetails']['town'] . '<br>';
			$towelradsMessage .= $dataToSend['towelrads_order']['data']['custDetails']['county'] . '<br>';
			$towelradsMessage .= $dataToSend['towelrads_order']['data']['custDetails']['postcode'] . '<br>';
			$towelradsMessage .= $dataToSend['towelrads_order']['data']['custDetails']['email'] . '<br>';
			$towelradsMessage .= $dataToSend['towelrads_order']['data']['custDetails']['phone'] . '<br><br>';

			foreach( $order->getAllItems() as $item ) {

				if ($item->getProductType() == 'configurable')
					continue;
				
				if ( strtolower( trim( $item->getSupplier() ) ) == 'towelrads' ) {

					$towelradsMessage .= (int) $item->getQtyOrdered() . ' x ' . $item->getSku() . ' - ' . $item->getName() . ' - (Supplier part number(s): ';

					$supplierPartNumber = str_replace(" ", "", $item->getSupplierPartNumber());
					$partNums = [];
					if(strpos($supplierPartNumber, '##') !== false) {
						
						$parts = explode("##", $supplierPartNumber);
						
						foreach( $parts as $part ) {							
							$partNums[] = $part;
						}
						
					} else {
						$partNums[] = $supplierPartNumber;
					}

					$towelradsMessage .= implode(', ', $partNums) . ')<br><br>';
				}
				
			}

			$towelradsVars = [
				'subject' => $dataToSend['towelrads_order']['data']['custRef'],
				'message' => $towelradsMessage,
			];
			// Added 16/08/2019 - Aaron ^^^
			
			$emailSent = $this->_mailer->sendEmail($towelradsVars, explode(",", str_replace(" ","",$this->_towelradsEmails)), $csv);

			if($emailSent) {
				$towelradsSuccess = true;
				$order->addStatusHistoryComment($this->_orderStatuses['ec_exported_tr'])->save();
				$trSaved = $this->_orders->saveOrder(null, $id, $attempts + 1, null, null, ['towelrads',true]);

				if($trSaved == false) {										
					$this->_messages[$id][] = ['message' => "Order has been exported to Towelrads but was unable to save results to the order exporter database.",'heading' => "Error Saving Data"];										
				}

			} else {
				// save unsuccessful attempt
				$order->setStatus('ec_retrying')->addStatusToHistory($order->getStatus())->save();
				$this->_orders->saveOrder(null, $id, $attempts + 1, false, null, ['towelrads',false]);
				$this->_logger->writeError("Order export unsuccessful - Unable to send email.", $id, ['data' => $dataToSend, 'response' => $emailSent]);
				$this->_messages[$id][] = ['message' => "Unable to export order to Towelrads at this time. The export will be attempted again the next time the export script runs.", 'heading' => "Unable To Export"];
			}

		} else {
			// save unsuccessful attempt
			$order->setStatus('ec_retrying')->addStatusToHistory($order->getStatus())->save();
			$this->_orders->saveOrder(null, $id, $attempts + 1, false, null, ['towelrads',false]);
			$this->_logger->writeError("Order export unsuccessful - Unable to create csv.", $id, ['data' => $dataToSend, 'response' => $emailSent]);
			$this->_messages[$id][] = ['message' => "Unable to export order to Towelrads at this time. The export will be attempted again the next time the export script runs.", 'heading' => "Unable To Export"];
		}

		unlink($csv);
		
		return $towelradsSuccess;
	}

	public function splitAddress($string) {
		if( strpos($string, "\n") !== false ) {

			$parts = array_map(function($p){ return str_replace("\r","",$p) ;}, explode("\n", $string));		
			
			if( count($parts) > 2 ) {

				$out = ['address1' => $parts[0] . " " . $parts[1]];
				
				unset($parts[0],$parts[1]);
				
				$out['address2'] = implode(" ", $parts);
				
				return $out;

			} else {
				
				return ['address1' => $parts[0], 'address2' => $parts[1]];
				
			}
		}
		return null;
	}
	
}
