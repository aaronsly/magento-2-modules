<?php
/**
 * Copyright © Essential E-Commerce Ltd, All rights reserved.
 */

namespace Essential\OrderExporter\Cron;

/**
 * Used to update Distinctive Wholesale's products stock levels
 * @version 1.0.0
 */

class Stock
{
	/**
     * Logging instance
     * @var \Essential\OrderExporter\Logger\Logger
     */
    protected $_logger;
	
	/**
     * Helper instance
     * @var \Essential\OrderExporter\Helper\Data
     */
	protected $_helper;
	
	/**
     * Mailer instance
     * @var \Essential\OrderExporter\Helper\Email
     */
	protected $_mailer;	
	
	/**
     * Dir instance
     * @var \Magento\Framework\Filesystem\DirectoryList
     */
	protected $_dir;
	
	/**
	 * Stock Registry instance
	 * @var \Magento\CatalogInventory\Api\StockRegistryInterface
	 */
	protected $_stockRegistry;
	
	/**
	* Product Repository Instance
	* @var \Magento\Catalog\Model\ProductRepository
	*/
	protected $_productRepository;
	
	/**
	* Product Resource Instance
	* @var \Magento\Catalog\Model\ResourceModel\Product
	*/
	protected $_productResource;
	
	/**
     * Error message array
     * @var array()
     */
	protected $_messages = [];
	
	/**
     * Path to stock feed csv file
     * @var string
     */
	public $_filepath;	
	
	/**
     * Path to stock feed csv file
     * @var string
     */
	public $_westFilepath;

	/**
     * Path to Towelrads stock feed csv file
     * @var string
     */
	public $_towelradsFilepath;	
	
	
	public function __construct(
		\Essential\OrderExporter\Helper\Data $helperData,
		\Essential\OrderExporter\Logger\Logger $logger,
		\Essential\OrderExporter\Helper\Email $mailer,
		\Magento\Framework\Filesystem\DirectoryList $dir,
		\Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
		\Magento\Catalog\Model\ProductRepository $productRepository,
		\Magento\Catalog\Model\ResourceModel\Product $productResource
	)
	{
			
		$this->_helper = $helperData;
		$this->_logger = $logger;
    	$this->_mailer = $mailer;
    	$this->_dir = $dir;    	
		$this->_stockRegistry = $stockRegistry;		
		$this->_productRepository = $productRepository;
		$this->_productResource = $productResource;
		$this->_filepath = $this->_dir->getRoot() . $this->_helper->getStockFeedPath();		
		$this->_westFilepath = $this->_dir->getRoot() . $this->_helper->getWestStockFeedPath();
		$this->_towelradsFilepath = $this->_dir->getRoot() . $this->_helper->getTowelradsStockFeedPath();
		
	}

	public function execute()
	{				
		if($this->_helper->stockFeedEnabled() && $this->_helper->stockExternalCron() == false) {
			$this->runDistinctiveFeed();
			$this->runWestFeed();
			$this->runTowelradsFeed();
			$this->sendErrorEmails();
		}

	} // execute end

	public function runTowelradsFeed() {

		$this->_logger->writeInfo("Towelrads stock feed start.");
		$flag = $this->_dir->getPath('tmp')."/towelrads-stock-feed.flag";
		
		if (file_exists($flag)) {
			
			$this->_logger->writeError("Towelrads stock feed is already running.");
			exit();
			
		}			

		file_put_contents($flag,"");
	
		if (file_exists($this->_towelradsFilepath)) {
		
			if(pathinfo($this->_towelradsFilepath)['extension'] == 'csv') {
				
				// Load variants-feed-west file to get sku's and associated supplier codes
				$path = $this->_dir->getPath('var')."/es_order_exporter_feeds/variants-feed-towelrads.json";
				
				if (file_exists($path)) {
					
					$searchSkus = json_decode(file_get_contents($path), true);

				} else {
					
					$this->_logger->writeError("Unable to load file: /var/es_order_exporter_feeds/variants-feed-towelrads.json");
					$this->_messages['file_error'][] = "Unable to load file: /var/es_order_exporter_feeds/variants-feed-towelrads.json";						
					
				}	

				if ($searchSkus) {
					$sortedStock = [];
					// Build assoc. array of csv data
					$rows = array_map('str_getcsv', file($this->_towelradsFilepath));					
					$header = ['product_code','qty'];
					$stockItems = array();

					foreach ($rows as $row) {					
						$sortedStock[strtolower($row[0])]['qty'] = $row[2];						
					}

					// Update stock levels
					foreach ($searchSkus as $sku => $partNum) {
						$dates = [];

						// Calculate product quantities						
						if(strpos($partNum, '##') !== false) {	

							// Get lowest product stock level of a bundled product
							$parts = explode('##',$partNum);
							$qtys = [];

							foreach ($parts as $part) {
								
								$partLower = strtolower($part);

								// If part number is for filling liquid assume that it's always in stock so skip
								if( $partLower == 400003 )
									continue;

								if( isset( $sortedStock[trim($partLower)] ) ) {

									$qtys[] = (int) $sortedStock[trim($partLower)]['qty'] >= 0 ? (int) $sortedStock[trim($partLower)]['qty'] : 0;

								} else {

									if( !isset( $this->_messages['stock_error'][$part] ) ) {
										$this->_logger->writeError("$part is not in the Towelrads stock feed - check supplier part number is correct.");
										$this->_messages['stock_error'][$part] = "$part is not in the Towelrads stock feed - check supplier part number is correct.";
									}
									
									$qtys[] = 0;

								}

							}
							
							$updateQty = min($qtys);

						} else {

							$partNumLower = strtolower($partNum);
							$skuLower = strtolower($sku);
							
							// Get standard product stock level
							if(isset($sortedStock[$partNumLower])) {

								$updateQty = (int) $sortedStock[$partNumLower]['qty'] >= 0 ? (int) $sortedStock[$partNumLower]['qty'] : 0;
								
							} elseif(isset($sortedStock[$skuLower])) {

								$updateQty = (int) $sortedStock[$skuLower]['qty'] >= 0 ? (int) $sortedStock[$skuLower]['qty'] : 0;
								
							} else {

								if( !isset($this->_messages['stock_error'][$partNum]) ) {
									$this->_logger->writeError("$partNum is not in the West Radiators stock feed - check supplier part number is correct.");
									$this->_messages['stock_error'][$partNum] = "$partNum is not in the West Radiators stock feed - check supplier part number is correct.";
								}								
								$updateQty = 0;

							}

						}

						// Set custom stock status message
						$stockMessage = $updateQty > 0 ? 'In Stock' : 'Out of Stock';

						// save custom stock status
						try {
							
							$prod = $this->_productRepository->get($sku);							
							$prod->setCustomStockStatus($stockMessage);
							$this->_productResource->saveAttribute($prod, 'custom_stock_status');
							
						} catch(\Magento\Framework\Exception\NoSuchEntityException $e) {

								$this->_logger->writeError($e->getMessage());
								$this->_messages['stock_error'][] = '<span style="font-weight:bold">Error: </span>'. $e->getMessage();			

						} catch(Exception $e) {

							$this->_logger->writeError("Problem loading product - " . $partNo, null, null, $e->getMessage() );
							$this->_messages['stock_error'][] = '<span style="font-weight:bold">Error: </span>Problem loading product - ' . $partNo;

						}

						// Load stock item by sku, update stock qty & set in stock value
						try {							

							$stockItem = $this->_stockRegistry->getStockItemBySku((string) $sku);						
							$stockItem->setQty((int) $updateQty);
							// $stockItem->setIsInStock((bool) $updateQty);
							$stockItem->setIsInStock(true);
							$this->_stockRegistry->updateStockItemBySku($sku, $stockItem);

						} catch(\Magento\Framework\Exception\NoSuchEntityException $e) {

								$this->_logger->writeError($e->getMessage());
								$this->_messages['stock_error'][] = '<span style="font-weight:bold">Error: </span>'. $e->getMessage() . '<br><span style="font-weight:bold">Product:</span> ' . $sku;			

						} catch(Exception $e) {

							$this->_logger->writeError("Problem loading product - " . $sku, null, null, $e->getMessage() );
							$this->_messages['stock_error'][] = '<span style="font-weight:bold">Error: </span>Problem loading product - ' . $sku;

						}

					}
					
				}

				unlink($this->_towelradsFilepath);	

			} else {

				$this->_logger->writeError("Stock feed file is not a .csv");
				$this->_messages['file_error'][] = "Stock feed file is not a .csv";

			}			

		} else {

			$this->_logger->writeError("No Towelrads stock feed file found.");

		}
		
		unlink($flag);

		$this->_logger->writeInfo("Towelrads stock feed end.");
	}

	public function runWestFeed() {

		$flag = $this->_dir->getPath('tmp')."/west-stock-feed.flag";
		
		if (file_exists($flag)) {
			
			$this->_logger->writeError("West stock feed is already running.");
			exit();
			
		}			

		file_put_contents($flag,"");
	
		if (file_exists($this->_westFilepath)) {
		
			if(pathinfo($this->_westFilepath)['extension'] == 'csv') {
				
				// Load variants-feed-west file to get sku's and associated supplier codes
				$path = $this->_dir->getPath('var')."/es_order_exporter_feeds/variants-feed-west.json";
				
				if (file_exists($path)) {
					
					$searchSkus = json_decode(file_get_contents($path), true);

				} else {
					
					$this->_logger->writeError("Unable to load file: /var/es_order_exporter_feeds/variants-feed-west.json");
					$this->_messages['file_error'][] = "Unable to load file: /var/es_order_exporter_feeds/variants-feed-west.json";						
					
				}	
				
				if ($searchSkus) {
					
					// Build assoc. array of csv data
					$rows = array_map('str_getcsv', file($this->_westFilepath));					
					$header = ['item_number','qty_on_hand'];
					$stockItems = array();

					foreach ($rows as $row) {							
						if(count($row) == 2 ) {
							$row[0] = str_replace("\0",'',$row[0]);
							$q = filter_var( $row[1], FILTER_SANITIZE_NUMBER_INT );
							$stockItems[] = array_combine($header, [$row[0], $q] );
						}
					}

					// Combine standard and ring fenced stock quantities
					$sortedStock = [];

					foreach($stockItems as $item) {

						$supplierPartNo = strtolower( $item['item_number'] );

						if(array_key_exists($supplierPartNo, $sortedStock)) {

							$sortedStock[$supplierPartNo]['qty'] += $item['qty_on_hand'];

						} else {

							$sortedStock[$supplierPartNo]['qty'] = $item['qty_on_hand'];

						}

					}

					// Update stock levels
					foreach ($searchSkus as $sku => $partNum) {
						$dates = [];
						// Calculate product quantities						
						if(strpos($partNum, '##') !== false) {	

							// Get lowest product stock level of a bundled product
							$parts = explode('##',$partNum);
							$qtys = [];

							foreach ($parts as $part) {
								
								$partLower = strtolower($part);

								if( isset( $sortedStock[trim($partLower)] ) ) {

									$qtys[] = (int) $sortedStock[trim($partLower)]['qty'];

								} else {

									if( !isset( $this->_messages['stock_error'][$part] ) ) {
										$this->_logger->writeError("$part is not in the West Radiators stock feed - check supplier part number is correct.");
										$this->_messages['stock_error'][$part] = "$part is not in the West Radiators stock feed - check supplier part number is correct.";
									}
									
									$qtys[] = 0;

								}

							}

							$updateQty = min($qtys);

						} else {

							$partNumLower = strtolower($partNum);
							$skuLower = strtolower($sku);
							
							// Get standard product stock level
							if(isset($sortedStock[$partNumLower])) {

								$updateQty = (int) $sortedStock[$partNumLower]['qty'];
								
							} elseif(isset($sortedStock[$skuLower])) {

								$updateQty = (int) $sortedStock[$skuLower]['qty'];
								
							} else {

								if( !isset($this->_messages['stock_error'][$partNum]) ) {
									$this->_logger->writeError("$partNum is not in the West Radiators stock feed - check supplier part number is correct.");
									$this->_messages['stock_error'][$partNum] = "$partNum is not in the West Radiators stock feed - check supplier part number is correct.";
								}								
								$updateQty = 0;

							}

						}
						
						// Set custom stock status message
						$stockMessage = $updateQty > 0 ? 'In Stock' : 'Out of Stock';

						// save custom stock status
						try {
							
							$prod = $this->_productRepository->get($sku);							
							$prod->setCustomStockStatus($stockMessage);
							$this->_productResource->saveAttribute($prod, 'custom_stock_status');
							
						} catch(\Magento\Framework\Exception\NoSuchEntityException $e) {

								$this->_logger->writeError($e->getMessage());
								$this->_messages['stock_error'][] = '<span style="font-weight:bold">Error: </span>'. $e->getMessage();			

						} catch(Exception $e) {

							$this->_logger->writeError("Problem loading product - " . $partNo, null, null, $e->getMessage() );
							$this->_messages['stock_error'][] = '<span style="font-weight:bold">Error: </span>Problem loading product - ' . $partNo;

						}

						// Load stock item by sku, update stock qty & set in stock value
						try {							

							$stockItem = $this->_stockRegistry->getStockItemBySku((string) $sku);						
							$stockItem->setQty((int) $updateQty);
							// $stockItem->setIsInStock((bool) $updateQty);
							$stockItem->setIsInStock(true);
							$this->_stockRegistry->updateStockItemBySku($sku, $stockItem);

						} catch(\Magento\Framework\Exception\NoSuchEntityException $e) {

								$this->_logger->writeError($e->getMessage());
								$this->_messages['stock_error'][] = '<span style="font-weight:bold">Error: </span>'. $e->getMessage() . '<br><span style="font-weight:bold">Product:</span> ' . $sku;			

						} catch(Exception $e) {

							$this->_logger->writeError("Problem loading product - " . $sku, null, null, $e->getMessage() );
							$this->_messages['stock_error'][] = '<span style="font-weight:bold">Error: </span>Problem loading product - ' . $sku;

						}

					}
					
				}

				unlink($this->_westFilepath);			

			} else {

				$this->_logger->writeError("Stock feed file is not a .csv");
				$this->_messages['file_error'][] = "Stock feed file is not a .csv";

			}			

		} else {

			$this->_logger->writeError("No stock feed file found.");
			// $this->_messages['file_error'][] = "No stock feed file found.";

		}
		
		unlink($flag);

	}

	public function runDistinctiveFeed() {

		$flag = $this->_dir->getPath('tmp')."/dw-stock-feed.flag";
		
		if (file_exists($flag)) {
			
			$this->_logger->writeError("Stock feed is already running.");
			exit();
			
		}			

		file_put_contents($flag,"");
	
		if (file_exists($this->_filepath)) {
		
			if(pathinfo($this->_filepath)['extension'] == 'csv') {
				
				// Load variants-feed file to get sku's and associated supplier codes
				$path = $this->_dir->getPath('var')."/es_order_exporter_feeds/variants-feed.json";
				
				if (file_exists($path)) {
					
					$searchSkus = json_decode(file_get_contents($path), true);

				} else {
					
					$this->_logger->writeError("Unable to load file: /var/es_order_exporter_feeds/variants-feed.json");
					$this->_messages['file_error'][] = "Unable to load file: /var/es_order_exporter_feeds/variants-feed.json";						
					
				}	
				
				if ($searchSkus) {
					
					// Build assoc. array of csv data
					$rows = array_map('str_getcsv', file($this->_filepath));
					$header = array_shift($rows);
					$stockItems = array();

					foreach ($rows as $row) {	

						$stockItems[] = array_combine($header, $row);	

					}

					// Combine standard and ring fenced stock quantities
					$sortedStock = [];

					foreach($stockItems as $item) {

						$supplierPartNo = strtolower( preg_replace( '/^TR-/', '', $item['variant_code'] ) );
						
						// catch trade rf only products- no main code sku in stock feed
						if(!$this->mainStock($stockItems, $supplierPartNo))
							$supplierPartNo = strtolower( $item['variant_code'] );

						if(array_key_exists($supplierPartNo, $sortedStock)) {

							$sortedStock[$supplierPartNo]['qty'] += $item['free_stock'];

						} else {

							$sortedStock[$supplierPartNo]['qty'] = $item['free_stock'];
							$sortedStock[$supplierPartNo]['name'] = str_replace("Trade Radiators - ", "", $item['variant_description']);

						}
						
						$sortedStock[$supplierPartNo]['due_dates'] = [];
						if ( $item['free_stock'] == 0 ) {
							$sortedStock[$supplierPartNo]['due_dates'][] = $item['port_eta'];
						}

					}

					// Update stock levels
					foreach ($searchSkus as $sku => $partNum) {
						$dates = [];
						// Calculate product quantities						
						if(strpos($partNum, '##') !== false) {	

							// Get lowest product stock level of a bundled product
							$parts = explode('##',$partNum);
							$qtys = [];

							foreach ($parts as $part) {
								
								$partLower = strtolower($part);

								if( isset( $sortedStock[trim($partLower)] ) ) {

									$qtys[] = (int) $sortedStock[trim($partLower)]['qty'];
									
									if ($sortedStock[$partLower]['qty'] == 0) {
										$dates = array_merge($dates,$sortedStock[$partLower]['due_dates']);
									}

								} else {

									if( !isset( $this->_messages['stock_error'][$part] ) ) {
										$this->_logger->writeError("$part is not in the stock feed - check supplier part number is correct.");
										$this->_messages['stock_error'][$part] = "$part is not in the stock feed - check supplier part number is correct.";
									}
									
									$qtys[] = 0;

								}

							}

							$updateQty = min($qtys);

						} else {

							$partNumLower = strtolower($partNum);
							$skuLower = strtolower($sku);

							// Get standard product stock level
							if(isset($sortedStock[$partNumLower])) {

								$updateQty = $sortedStock[$partNumLower]['qty'];
								
								if ( $sortedStock[$partNumLower]['qty'] == 0 )
									$dates = array_merge($dates,$sortedStock[$partNumLower]['due_dates']);

							} elseif(isset($sortedStock[$skuLower])) {
								$updateQty = $sortedStock[$skuLower]['qty'];
								
								if ( $sortedStock[$skuLower]['qty'] == 0 )
									$dates = array_merge($dates,$sortedStock[$skuLower]['due_dates']);
							} else {

								if( !isset($this->_messages['stock_error'][$partNum]) ) {
									$this->_logger->writeError("$partNum is not in the stock feed - check supplier part number is correct.");
									$this->_messages['stock_error'][$partNum] = "$partNum is not in the stock feed - check supplier part number is correct.";
								}								
								$updateQty = 0;

							}

						}
						
						// Get longest due date
						$dueDate = 0;
						if ( $updateQty == 0 ) {
							
							foreach( $dates as $date ) {
								
								if (!empty($date)) {	
									
									$currentDate = strtotime(str_replace("/","-",$date) );
									
									if ( $currentDate > $dueDate) 
										$dueDate = $currentDate;	
									
								}
								
							}
							
						}
						
						// Set custom stock status message
						if( $updateQty > 0 ) {
							$stockMessage = 'In Stock';
						} elseif ( $updateQty == 0 && $dueDate == 0 ) {
//							$stockMessage = 'Out of Stock';
							$stockMessage = 'Due In: T.B.C';

						} elseif($dueDate) {
//							$stockMessage = 'Back In Stock: ' . date("d/m/Y",$dueDate);
							$stockMessage = 'Due In: ' . date("d/m",$dueDate);

						}						

						// save custom stock status
						try {
							
							$prod = $this->_productRepository->get($sku);							
							$prod->setCustomStockStatus($stockMessage);
							$this->_productResource->saveAttribute($prod, 'custom_stock_status');
							
						} catch(\Magento\Framework\Exception\NoSuchEntityException $e) {

								$this->_logger->writeError($e->getMessage());
								$this->_messages['stock_error'][] = '<span style="font-weight:bold">Error: </span>'. $e->getMessage();			

						} catch(Exception $e) {

							$this->_logger->writeError("Problem loading product - " . $partNo, null, null, $e->getMessage() );
							$this->_messages['stock_error'][] = '<span style="font-weight:bold">Error: </span>Problem loading product - ' . $partNo;

						}

						// Load stock item by sku, update stock qty & set in stock value
						try {							

							$stockItem = $this->_stockRegistry->getStockItemBySku((string) $sku);						
							$stockItem->setQty((int) $updateQty);
							// $stockItem->setIsInStock((bool) $updateQty);
							$stockItem->setIsInStock(true);
							$this->_stockRegistry->updateStockItemBySku($sku, $stockItem);

						} catch(\Magento\Framework\Exception\NoSuchEntityException $e) {

								$this->_logger->writeError($e->getMessage());
								$this->_messages['stock_error'][] = '<span style="font-weight:bold">Error: </span>'. $e->getMessage() . '<br><span style="font-weight:bold">Product:</span> ' . $sku;			

						} catch(Exception $e) {

							$this->_logger->writeError("Problem loading product - " . $sku, null, null, $e->getMessage() );
							$this->_messages['stock_error'][] = '<span style="font-weight:bold">Error: </span>Problem loading product - ' . $sku;

						}

					}
					
				}				

			} else {

				$this->_logger->writeError("Stock feed file is not a .csv");
				$this->_messages['file_error'][] = "Stock feed file is not a .csv";

			}			

		} else {

			$this->_logger->writeError("No stock feed file found.");
			$this->_messages['file_error'][] = "No stock feed file found.";

		}

		// Send error emails
		$this->sendErrorEmails();
		
		unlink($flag);

	}

	public function sendErrorEmails() {
		
		if ($this->_messages) {

			$html = '';

			foreach($this->_messages as $type => $messages) {

				if($type == 'stock_error') {
					
					$html .= '<h3 style="text-decoration:underline;">Product Stock Read / Write Errors:</h3>';
					$html .= '<p>' . implode("<br><br>", $messages) . '</p>';
					
				}

				if($type == 'file_error') {
					
					$html .= '<h3 style="text-decoration:underline;">File Error:</h3>';
					$html .= '<p>' . implode("<br><br>", $messages) . '</p>';
					
				}

			}

			$vars = ['subject' => "Stock Feed Error" , 'message' => $html];
			$this->_mailer->sendErrorEmails($vars);

		}

	}
	
	public function mainStock($allItems,$plainSku) {
		
		$skus = [];
		
		foreach( $allItems as $item ) {
			$skus[] = strtolower( trim($item['variant_code']) );
		}
		
		$result = in_array(trim($plainSku), $skus);		
		
		return $result;
	}
	
	
}
