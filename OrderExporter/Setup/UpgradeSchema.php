<?php
/**
 * Copyright © Essential E-Commerce Ltd, All rights reserved.
 */

namespace Essential\OrderExporter\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeSchema implements UpgradeSchemaInterface {

    private $eavSetupFactory;

    public function __construct(
        \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
    }
 
    public function upgrade( SchemaSetupInterface $setup, ModuleContextInterface $context ) {
         
        $setup->startSetup();
        
        if(version_compare($context->getVersion(), '0.0.2', '<')) {
            
			/**
			* Add custom order statuses
			*/
			$data = array( 
				['status' => 'ec_exported_dw', 'label' => 'Exported To Distinctive Wholesale'],
				['status' => 'ec_exported_west', 'label' => 'Exported To West Radiators'],
							);
			$setup->getConnection()->insertArray($setup->getTable('sales_order_status'), ['status', 'label'], $data);

			$setup->getConnection()->insertArray(
				$setup->getTable('sales_order_status_state'),
				['status', 'state', 'is_default','visible_on_front'],
				[
					['ec_exported_dw','processing', '0', '1'], 
					['ec_exported_west', 'processing', '0', '1'],					
				]
			);
			
			
			// add extra supplier columns
			$setup->getConnection()->addColumn(
                $setup->getTable('essential_order_exporter'),
                'distinctive_exported',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                    'nullable' => true,
                    'comment' => 'Exported to Distinctive Wholesale',
                    'after' => 'export_attempts'
                ]
            );
			$setup->getConnection()->addColumn(
                $setup->getTable('essential_order_exporter'),
                'west_exported',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                    'nullable' => true,
                    'comment' => 'Exported to West Radiators',
                    'after' => 'dw_exported'
                ]
            );            
			
        }

        if (version_compare($context->getVersion(), '0.0.3', '<')) {
            $this->addTowelRads($setup);
        }
 
        $setup->endSetup();
    }

    /**
     * @param SchemaSetupInterface $setup
     * @throws \Zend_Db_Exception
     */
    public function addTowelRads(SchemaSetupInterface $setup) {

        /**
        * Add custom order status
        */
        $data = array( 
            ['status' => 'ec_exported_tr', 'label' => 'Exported To Towelrads'],
        );
        $setup->getConnection()->insertArray($setup->getTable('sales_order_status'), ['status', 'label'], $data);

        $setup->getConnection()->insertArray(
            $setup->getTable('sales_order_status_state'),
            ['status', 'state', 'is_default','visible_on_front'],
            [
                ['ec_exported_tr','processing', '0', '1'],
            ]
        );
        
        
        // add extra supplier column
        $setup->getConnection()->addColumn(
            $setup->getTable('essential_order_exporter'),
            'towelrads_exported',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                'nullable' => true,
                'comment' => 'Exported to Towelrads',
                'after' => 'west_exported'
            ]
        );      

    }
}