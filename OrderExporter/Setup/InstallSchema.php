<?php
/**
 * Copyright © Essential E-Commerce Ltd, All rights reserved.
 */

namespace Essential\OrderExporter\Setup;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;


class InstallSchema implements InstallSchemaInterface
{
	
	public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
	{
		$setup->startSetup();
		
		/**
		* Create table 'essential_order_exporter'
		*/
		$table = $setup->getConnection()
			->newTable($setup->getTable('essential_order_exporter'))
			->addColumn(
				'id',
				\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
				null,
				['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
				'Id'
			)->addColumn(
				'order_id',
				\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
				255,
				['nullable' => false, 'default' => ''],
				'Magento Order Id'
			)->addColumn(
				'ignore',
				\Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
				null,
				[],
				'Ignore Order'
			)->addColumn(
				'exported',
				\Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
				null,
				[],
				'Export Result'
			)->addColumn(
				'export_attempts',
				\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
				11,
				['nullable' => false],
				'Export Attempts'
			)->addColumn(
				'created_at',
				\Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
				null,
				['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
				'Created At'
			)->addColumn(
				'updated_at',
				\Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
				null,
				['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
				'Updated At'
			)->setComment("Exported Orders table");
		$setup->getConnection()->createTable($table);
		
		/**
		* Add custom order statuses
		*/
		$data = array( 
			['status' => 'ec_exported', 'label' => 'Exported To Distinctive Wholesale'],
			['status' => 'ec_retrying', 'label' => 'Retrying Export'],
			['status' => 'ec_export_failed', 'label' => 'Unable To Export']
		);
        $setup->getConnection()->insertArray($setup->getTable('sales_order_status'), ['status', 'label'], $data);
 
        $setup->getConnection()->insertArray(
			$setup->getTable('sales_order_status_state'),
			['status', 'state', 'is_default','visible_on_front'],
			[
				['ec_exported','processing', '0', '1'], 
				['ec_retrying', 'processing', '0', '1'],
				['ec_export_failed', 'processing', '0', '1']
			]
        );
		
		$setup->endSetup();		
	}
}
