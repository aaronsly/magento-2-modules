<?php
/**
 * Copyright © Essential E-Commerce Ltd, All rights reserved.
 */

namespace Essential\OrderExporter\Observer;
 
use Magento\Framework\Event\ObserverInterface;

/**
 * Essential Order Exporter - Sets supplier on quote item
 * @version 1.0.0
 */

class SetItemSupplier implements ObserverInterface
{
	/**
     * Logging instance
     * @var \Essential\OrderExporter\Logger\Logger
     */
    protected $_logger;
	
	/**
     * Logging instance
     * @var \Essential\OrderExporter\Logger\Logger
     */
    protected $_helper;
	

    public function __construct(
        \Essential\OrderExporter\Logger\Logger $logger,
		\Essential\OrderExporter\Helper\Data $helperData
    ) {
        $this->_logger = $logger;
		$this->_helper = $helperData;
    }
	
    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $quoteItem = $observer->getQuoteItem();
        $product = $observer->getProduct();		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$product_data = $objectManager->create('Magento\Catalog\Model\Product')->load($product->getId());		
        $quoteItem->setSupplier($product_data->getData('supplier'));
        $quoteItem->setSupplierPartNumber($product_data->getData('supplier_part_number'));
		
    }
}