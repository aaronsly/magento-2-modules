<?php

namespace Essential\TechImg\Plugin;
 
class GalleryPlugin
{
    protected $moduleAssetDir;

    public function __construct(
        \Magento\Framework\View\Asset\Repository $moduleAssetDir
    ) {       
        $this->moduleAssetDir = $moduleAssetDir;       
    }

    public function afterGetGalleryImagesJson(\Magento\Catalog\Block\Product\View\Gallery $subject, $result)
    {
        
        $images = json_decode($result,true);

        foreach( $images as &$image) {
            if( strpos( $image['img'], 'technical_' ) !== false || strpos( $image['img'], 'technical' ) !== false ) {                
                $image['thumb'] = $this->moduleAssetDir->getUrl("Essential_TechImg::images/technical.jpg");;
            }
        }

        return json_encode($images);
    }
}