<?php
/**
 * Copyright © Essential E-Commerce Ltd, All rights reserved.
 */

namespace Essential\VideoImporter\Cron;

/**
 * Assigns Vimeo and YouTube videos to products based on an attribute
 * @version 1.0.0
 */

class VideoImporter
{
    
    /**
     * Curl instance
     * @var \Magento\Framework\HTTP\Client\Curl
     */
    protected $_curl;   

    /**
     * DirectoryList instance
     * @var \Magento\Framework\Filesystem\DirectoryList
     */
    protected $_dir;
    
    /**
     * File instance
     * @var \Magento\Framework\Filesystem\Io\File
     */
    protected $_file;

    /**
     * Helper instance
     * @var \Essential\OrderExporter\Helper\Data
     */
    protected $_helper;

    /**
     * LoggerInterface instance
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;
    
    /**
     * Product Collection instance
     * @var \Magento\Catalog\Model\ResourceModel\Product\Collection
     */    
    protected $_productResource;

    /**
     * Gallery Processor instance
     * @var \Essential\VideoImporter\Model\Product\Gallery\Video\Processor
     */
	protected $_videoGalleryProcessor;    

    /**
     * Title for logged errors
     * @var string
     */
    private $errorTitle;

    /**
     * YouTube API Key
     * @var string
     */
    private $youTubeKey;

    /**
     * VideoImport constructor.
     * 
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\HTTP\Client\Curl $curl
     * @param \Magento\Framework\Filesystem\DirectoryList $dir
     * @param \Magento\Framework\Filesystem\Io\File $file
     * @param \Essential\VideoImporter\Helper\Data $helperData
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $productResource
     * @param \Essential\VideoImporter\Model\Product\Gallery\Video\Processor $videoGalleryProcessor
     * 
     */
    public function __construct(        
        \Magento\Framework\HTTP\Client\Curl $curl,
        \Magento\Framework\Filesystem\DirectoryList $dir,
        \Magento\Framework\Filesystem\Io\File $file,
        \Essential\VideoImporter\Helper\Data $helperData,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Catalog\Model\ResourceModel\Product\Collection $productResource,
        \Essential\VideoImporter\Model\Product\Gallery\Video\Processor $videoGalleryProcessor
        
    ) {
        
        $this->_curl = $curl;
        $this->_dir = $dir;	
        $this->_file = $file;
        $this->_helper = $helperData;
        $this->_logger = $logger;
        $this->_productResource = $productResource;
        $this->_videoGalleryProcessor = $videoGalleryProcessor; 
                
        $this->errorTitle = 'Essential_VideoImporter: ';
        $this->youTubeKey = $this->_helper->getConfigValue('catalog/product_video/youtube_api_key');

    }

    public function execute()
    {

        $flag = $this->_dir->getPath('tmp')."/video-import.flag";
		
        if ( file_exists( $flag ) ) {
            $this->_logger->writeError( "Video importer is already running." );
            exit();
        }			

        file_put_contents($flag,"");
            
        $thumbsPath = $this->_dir->getPath('media')."/video-thumbs/";

        if ( !is_dir( $thumbsPath ) )
            $this->_file->mkdir( $thumbsPath, 0775 );  
        
        // Load all products with values for video links attribute
        $productCollection = $this->_productResource
                            ->addAttributeToSelect('*')
                            ->addAttributeToFilter('video_links', ['notnull' => true])
                            ->addMediaGalleryData()
                            ->load();

        if( $productCollection ) {

            foreach( $productCollection as $product ) {

                $videoLinks = explode( ',',$product->getVideoLinks() );

                // Get Existing product videos to avoid adding duplicates
                $galleryImages = $product->getMediaGalleryImages();                
                $existingVids = [];
                $position = count( $galleryImages );

                if ($galleryImages) {

                    foreach ($galleryImages as $image) {

                        $imageData = $image->getData();

                        if ( isset( $imageData['media_type'] ) && $imageData['media_type'] == 'external-video' ) {

                            $id = $this->getIdFromUrl( $imageData['video_url'] );

                            if( $id )
                                $existingVids[$id] = $imageData["position"];

                        }
                    }
                }

                // Add each link as a video on the product
                foreach( $videoLinks as $link ) {

                    $link = trim( $link );
                    $videoId = $this->getIdFromUrl( $link );
                    $position++;
                    
                    // If video exists on product skip
                    if( isset( $existingVids[$videoId] ) )
                        continue;

                    $videoData = [
                        'media_type' => \Magento\ProductVideo\Model\Product\Attribute\Media\ExternalVideoEntryConverter::MEDIA_TYPE_CODE,
                        'video_url'  => $link,
                        'video_id'   => $videoId,
                        'position'   => $position,
                    ];                   

                    if( strpos( $link, 'youtube' ) !== false ) {

                        $apiInfo = $this->getYoutubeData( $videoId );

                        if( $apiInfo ) {

                            $videoData = array_merge( $videoData, [
                                'thumbnail'         => filter_var( $apiInfo->items[0]->snippet->thumbnails->standard->url, FILTER_SANITIZE_URL ),
                                'video_description' => filter_var( $apiInfo->items[0]->snippet->description, FILTER_SANITIZE_STRING ),
                                'video_metadata'    => null,
                                'video_provider'    => 'youtube',
                                'video_title'       => filter_var( $apiInfo->items[0]->snippet->title, FILTER_SANITIZE_STRING ),
                            ] );

                        } else {
                            
                            $this->_logger->error( $this->errorTitle . "HTTP request failed. Check YouTube video id and url are correct - Video id: $videoId, Video URL: $link" );
                            $videoData = false;

                        }                        

                    } elseif( strpos( $link, 'vimeo' ) !== false ) {

                        $apiInfo = $this->getVimeoData($videoId);

                        if( $apiInfo ) {

                            $videoData = array_merge( $videoData, [
                                'thumbnail'         => filter_var( $apiInfo->thumbnail_large, FILTER_SANITIZE_URL ),
                                'video_description' => filter_var( $apiInfo->description, FILTER_SANITIZE_STRING ),
                                'video_metadata'    => null,
                                'video_provider'    => 'vimeo',
                                'video_title'       => filter_var( $apiInfo->title, FILTER_SANITIZE_STRING ),
                            ] ); 

                        } else {

                            $this->_logger->error( $this->errorTitle . "HTTP request failed. Check Vimeo video id and url are correct - Video id: $videoId, Video URL: $link" );
                            $videoData = false;

                        }                      

                    } else {

                        $this->_logger->error( $this->errorTitle . "Link is not a Vimeo or YouTube Link. Check link is correct format: $link" );
                        $videoData = false;

                    }

                    if( $videoData ) {

                        //download thumbnail image and save locally under pub/media
                        $filename = $videoData['video_id'] . '.jpg';

                        if( !file_exists( $thumbsPath . $filename ) )                            
                            copy( $videoData['thumbnail'], $thumbsPath . $filename );
                        
                        // Add video to the product
                        $videoData['file'] = 'video-thumbs/' . $filename;                        
                        
                        if ( $product->hasGalleryAttribute() )
                            $this->_videoGalleryProcessor->addVideo( $product, $videoData, ['image', 'small_image', 'thumbnail'], false, false );
                        
                        $product->save();

                    }                   

                }

                sleep( 5 );
                
            }

        } else {

            $this->_logger->error( $this->errorTitle . "Unable to load products with video links." );

        }

        unlink($flag);
		
    } // execute() end


    /*
	* Get Vimeo video data via api
	*
	* @param string $id
	*
	* @return object / bool
	*/
    public function getVimeoData( $id ) {

        $url = 'https://www.vimeo.com/api/v2/video/' . $id . '.json';

        if ( $this->get_http_response_code( $url ) != '200' ) {

            return false;

        } else {      

            $data = json_decode( file_get_contents( $url ) );            
            return reset( $data );

        }

    }


    /*
	* Get YouTube video data via api
	*
	* @param string $id
	*
	* @return object / bool
	*/
    public function getYoutubeData( $id ) {

        $url = 'https://www.googleapis.com/youtube/v3/videos?id=' . $id . '&part=snippet,contentDetails,statistics,status&key=' . $this->youTubeKey;

        if ( $this->get_http_response_code( $url ) != '200' ) {

            return false;

        } else {     

            return json_decode( file_get_contents( $url ) );

        }

    }

    /*
	* Gets video id from url
	*
	* @param string $url
	*
	* @return object / bool
	*/
    public function getIdFromUrl( $url ) {

        if( strpos( $url, 'youtube' ) !== false ) {

            return substr( $url, strpos( $url, '?v=' ) + 3 );

        } elseif( strpos( $url, 'vimeo' ) !== false ) {

            return substr( $url, strpos( $url, '.com/' ) + 5 ); 

        } else {

            return false;

        }

    }

    /*
	* Checks HTTP response for api url and returns response code
	*
	* @param string $url
	*
	* @return string
	*/
    public function get_http_response_code( $url ) {

        $headers = get_headers( $url );

        if( isset( $headers[21] ) ) {

            return substr( $headers[21], 9, 3 );

        } else {

            return substr( $headers[0], 9, 3 );

        }
        
    }
	
} // class end
