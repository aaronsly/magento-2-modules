<?php
/**
 * Copyright © Essential E-Commerce Ltd, All rights reserved.
 */

namespace Essential\OrderExporter\Cron;

/**
 * Used to generate feed file of sku's and matching supplier part number for Distinctive Wholesale products
 * @version 1.0.0
 */

class Variants
{
	/**
     * Logging instance
     * @var \Essential\OrderExporter\Logger\Logger
     */
    protected $_logger;
	
	/**
     * Helper instance
     * @var \Essential\OrderExporter\Helper\Data
     */
	protected $_helper;
	
	/**
     * Dir instance
     * @var \Magento\Framework\Filesystem\DirectoryList
     */
	protected $_dir;
	
	/**
     * File instance
     * @var \Magento\Framework\Filesystem\Io\File
     */
	protected $_file;
	
	/**
     * Product Collection instance
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
	protected $_productCollection;
	
	
	public function __construct(
		\Essential\OrderExporter\Helper\Data $helperData,
		\Essential\OrderExporter\Logger\Logger $logger,
		\Magento\Framework\Filesystem\DirectoryList $dir,
		\Magento\Framework\Filesystem\Io\File $file,
		\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollection)
	{		
			
		$this->_helper = $helperData;
		$this->_logger = $logger;
    	$this->_dir = $dir;	
    	$this->_file = $file;
   	 	$this->_productCollection = $productCollection;
		
	}

	public function execute()
	{
		
		if($this->_helper->stockFeedEnabled() || $this->_helper->exportsEnabled()) {
			
			if ($this->_helper->variantsExternalCron() == false) {


				$this->createDistinctiveVariants();

				$this->createWestVariants();

				$this->createTowelradsVariants();
				
			}		

		}

	} // execute end

	public function createDistinctiveVariants() {

		// Load all Distinctive Wholesale products
		try {

			$collection = $this->_productCollection->create();
			// $collection->setFlag('has_stock_status_filter', true);
			$collection->addAttributeToSelect('*');
			$collection->addAttributeToFilter('status',\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
			$collection->addAttributeToFilter('supplier',array('in'=> ['distinctive wholesale', 'distinctive wholesale ltd', 'distinctive']));

		} catch( Exception $e ) {

			$this->_logger->writeError("Problem loading products - in " . __FILE__, null, null, $e->getMessage());
			exit();

		}			

		// Create data for feed file
		$data = [];
		foreach($collection as $product) {

			$partNum = $product->getSupplierPartNumber();

			if (!empty($partNum)){

				$data[$product->getSku()] = $partNum;

			}		

		}			

		// Save feed file
		try {

			$path = $this->_dir->getPath('var')."/es_order_exporter_feeds/";

			if (!is_dir($path)) {
				$this->_file->mkdir($path, 0775);
			}

			$this->_file->open(array('path' => $path));
			$this->_file->write('variants-feed.json', json_encode($data));

		} catch (Exception $e) {

			$this->_logger->writeError("Unable to write data to feed file - in " . __FILE__, null, null, $e->getMessage());
			exit();

		}
		
	} // createDistinctiveVariants() end


	public function createWestVariants() {

		// Load all West products
		try {

			$collection = $this->_productCollection->create();
			// $collection->setFlag('has_stock_status_filter', true);
			$collection->addAttributeToSelect('*');
			$collection->addAttributeToFilter('status',\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
			$collection->addAttributeToFilter('supplier',array('in'=> ['west', 'west radiators']));

		} catch( Exception $e ) {

			$this->_logger->writeError("Problem loading products - in " . __FILE__, null, null, $e->getMessage());
			exit();

		}			

		// Create data for feed file
		$data = [];
		foreach($collection as $product) {

			$partNum = $product->getSupplierPartNumber();

			if (!empty($partNum)){
				$data[$product->getSku()] = $partNum;
			}								
		}			

		// Save feed file
		try {

			$path = $this->_dir->getPath('var')."/es_order_exporter_feeds/";

			if (!is_dir($path)) {
				$this->_file->mkdir($path, 0775);
			}

			$this->_file->open(array('path' => $path));
			$this->_file->write('variants-feed-west.json', json_encode($data));

		} catch (Exception $e) {

			$this->_logger->writeError("Unable to write data to feed file - in " . __FILE__, null, null, $e->getMessage());
			exit();

		}

	} // createWestVariants() end

	public function createTowelradsVariants() {

		// Load all Towelrads products
		try {

			$collection = $this->_productCollection->create();
			$collection->setFlag('has_stock_status_filter', true);
			$collection->addAttributeToSelect('*');
			$collection->addAttributeToFilter('status',\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
			$collection->addAttributeToFilter('supplier',array('in'=> ['towelrads']));

		} catch( Exception $e ) {

			$this->_logger->writeError("Problem loading products - in " . __FILE__, null, null, $e->getMessage());
			exit();

		}			

		// Create data for feed file
		$data = [];
		foreach($collection as $product) {
			if (!empty($product->getSupplierPartNumber())){
				$data[$product->getSku()] = $product->getSupplierPartNumber();
			}								
		}			

		// Save feed file
		try {

			$path = $this->_dir->getPath('var')."/es_order_exporter_feeds/";

			if (!is_dir($path)) {
				$this->_file->mkdir($path, 0775);
			}

			$this->_file->open(array('path' => $path));
			$this->_file->write('variants-feed-towelrads.json', json_encode($data));

		} catch (Exception $e) {

			$this->_logger->writeError("Unable to write data to feed file - in " . __FILE__, null, null, $e->getMessage());
			exit();

		}

	} //createTowelradsVariants() end
	
}
