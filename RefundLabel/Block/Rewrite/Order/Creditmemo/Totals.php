<?php 
namespace Essential\RefundLabel\Block\Rewrite\Order\Creditmemo;

use Essential\RefundLabel\Block\Rewrite\Order\Creditmemo;
 
class Totals extends \Magento\Sales\Block\Order\Creditmemo\Totals
{
    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        parent::__construct($context, $registry, $data);
        $this->_isScopePrivate = true;
    }
 
    /**
     * Initialize order totals array
     *
     * @return $this
     */
    protected function _initTotals()
    {
        parent::_initTotals();
        $parent = $this->getParentBlock();
        $source = $parent->getSource();

        $this->removeTotal('base_grandtotal');
        if ((double)$this->getSource()->getAdjustmentPositive()) {
            $total = new \Magento\Framework\DataObject(
                [
                    'code' => 'adjustment_positive',
                    'value' => $this->getSource()->getAdjustmentPositive(),
                    'label' => __('Adjustment Refund'),
                ]
            );
            $this->addTotal($total);
        }
        if ((double)$this->getSource()->getAdjustmentNegative()) {
            $total = new \Magento\Framework\DataObject(
                [
                    'code' => 'adjustment_negative',
                    'value' => $this->getSource()->getAdjustmentNegative(),
                    'label' => __('Adjustment Fee'),
                ]
            );
            $this->addTotal($total);
        }
        
        $this->removeTotal('grand_total');
        $total = new \Magento\Framework\DataObject(
                [
                    'code' => 'grand_total',
                    'field' => 'grand_total',
                    'strong' => true, 
                    'value' => $this->getSource()->getGrandTotal(),
                    'label' => __('Refund Total'),
                ]
            );
        $this->addTotal($total, 'last');
        return $this;
    }

}