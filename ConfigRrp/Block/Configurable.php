<?php
namespace Essential\ConfigRrp\Block;
class Configurable extends \Magento\Framework\View\Element\Template
{
    public $confProd;
    public $helper;
    public $jsonEncoder;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\ConfigurableProduct\Block\Product\View\Type\Configurable $confProd,
        \Magento\ConfigurableProduct\Helper\Data $helper,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder
        )
	{
        parent::__construct($context);
        $this->confProd = $confProd;
        $this->helper = $helper;
        $this->jsonEncoder = $jsonEncoder;
	}
    
    public function getChildProducts() {
        return $this->confProd->getProduct()->getTypeInstance()->getUsedProducts($this->confProd->getProduct());
    }

    public function esJsConfig() {
        
        $options = $this->helper->getOptions( $this->confProd->getProduct(), $this->confProd->getAllowProducts());

        $config = [];
        foreach( $this->getChildProducts() as $child) {
            $key = reset($options['index'][$child->getId()]);
            
            $config[$key] = [
                'rrp' => (float) $child->getRrp(),
                'product_id' => $child->getId(),
                'super_attr' => $key
            ];
        }
        return $this->jsonEncoder->encode($config);
    }
}